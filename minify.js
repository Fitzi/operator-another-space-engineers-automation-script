const fs = require('fs');
const readline = require('node:readline');
const util = require('util');

const watch = process.argv.includes('--watch');



const sourceFileName = "Program.cs";
const targetFileName = "Program.min.cs";
const startDelim = "#region script";
const endDelim = "// end of script";

let locale = Intl.DateTimeFormat().resolvedOptions().locale;

const minifyAndCopyFile = () => {
    const sourceFileStream = fs.createReadStream(sourceFileName);
    const targetFileStream = fs.createWriteStream(targetFileName, { flags: "w" });

    const soureFileLines = readline.createInterface({
        input: sourceFileStream,
        crlfDelay: Infinity
    });

    const showOutput = process.argv.includes('--print-skipped-lines');

    let startFound = false;
    let endFound = false;

    let inComment = false;

    let i = 0;
    let skippedLines = 0;
    let skippedLength = 0;

    let scriptLength = 0;
    const maxScriptLength = 100000;

    const log = (text) => {
        if( showOutput ){
            console.log(text);
        }
    }

    const logSkip = (line) => {
        log(`Skipping line ${i}: ${line}`);
        skippedLines++;
        skippedLength += line.length + 1; // +1 for line break
    }

    soureFileLines.on('line', (line) => {
        i++;
    
        // add back line breaks
        const trimmedLine = line.trim();

        // check for start delimiter
        if( !startFound ){
            logSkip(line);

            if( trimmedLine === startDelim ){
                log('Start found');
                startFound = true;
            }
            return;
        }

        // check for end delimiter
        if( !endFound ){
            if( trimmedLine === endDelim ){
                log('End found');
                endFound = true;
                return logSkip(line);
            }
        }else{
            return logSkip(line);
        }

        // skip single line comments, regions and empty lines
        if( trimmedLine.startsWith("//") || trimmedLine.startsWith("#region") || trimmedLine.startsWith("#endregion") ||trimmedLine.length === 0 ){
            return logSkip(line);
        }

        // detect multi line comments
        if( trimmedLine.startsWith("/*") ){
            inComment = true;
            return logSkip(line);
        }

        if( trimmedLine.startsWith("*/") ){
            inComment = false;
            return logSkip(line);
        }

        if( inComment ){
            return logSkip(line);
        }

        skippedLength += line.length - trimmedLine.length;
        scriptLength += trimmedLine.length + 1; // +1 for line breaks

        // write line
        targetFileStream.write(trimmedLine + "\n");
    });

    soureFileLines.on('close', () => {
        console.log(`Skipped ${skippedLines} lines and a total of ${skippedLength} characters.`);
        console.log(`Current script length is ${scriptLength.toLocaleString(locale)}/${maxScriptLength.toLocaleString(locale)} (${ Math.floor( (scriptLength / maxScriptLength) * 100 ) }%) characters`);
        targetFileStream.close(() => {
            const contents = fs.readFileSync(targetFileName, { encoding: 'utf8'});
            require('child_process').spawn('clip', [], { encoding: "utf8" }).stdin.end(contents);
            console.log('Script copied to clipboard');
        });

    });
};


if( watch ){
    let debounce;

    fs.watch( sourceFileName, (eventType) => {
        if( eventType !== "change"){ return };
        clearTimeout(debounce);
        debounce = setTimeout( () => {
            minifyAndCopyFile();
        }, 100);
    });

}else{
    minifyAndCopyFile();
}
