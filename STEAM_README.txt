OPERATOR combines a bunch of useful features, like displaying information around resources (hydrogen, oxygen, batteries, etc), your ships status, etc.
It also adds automations like self-closing/locking airlocks and hangar doors.
For detailed information about usage and features, check out the [url=https://gitlab.com/Fitzi/operator-another-space-engineers-automation-script]GitLab repository[/url]
All the scripts functionality is controlled via the CustomData properties.

[h1]Script settings[/h1]
The script settings are defined in the programmable blocks CustomData.
The format is [i]KEY:VALUE[/i] (one per line) where KEY is one of the following values:

[list]
[*][i]mode[/i]: Determines the mode the script runs in. Possible values are [i]Main[/i], [i]Miner[/i], [i]Fighter[/i]. Defaults to [i]Main[/i].
[*][i]speed[/i]: Sets the ships intended max speed. Currently only used to display a speed bar on environment displays. Defaults to 100.
[*][i]airlock_delay[/i]: Sets the number of ticks a doors in an airlock stays open. Defaults to 20. (Must be multiple of 10; 1 second = 60 ticks)
[/list]

[h1]Updating the script[/h1]
The script only searches blocks and processes custom data on startup to safe on ressources. 
After changing the base, adding lcds or changing custom data, run [i]init[/i] or recompile the script.

[h1]Display information on LCDs[/h1]
On any LCD, you can display any of the listed information by adding [i]display:TYPE[/i] to the displays custom data, where [i]TYPE[/i] can be any of the following values:

[list]
[*][i]hydrogen[/i]: Displays max. total capacity, available capacity, current fill, stockpile warnings, etc.
[*][i]oxygen[/i]: Same info as hydrogen, just for oxygen
[*][i]energy[/i]: Summarizes energy production. Will show current output percentage of batteries, solar, reactors and other. Also shows warning for low uranium levels.
[*][i]batteries[/i]: Displays detailed information for batteries like current charge level, max capacity, current in/output, mode warnings, etc.
[*][i]solar[/i]:  Displays detailed information like current output, max possible output (depending on the sun angle) and max total output
[*][i]reactor[/i]: Displays detailed information for reactors like current and max reactor output and uranium info.
[*][i]environment[/i]: Displays natural gravity, outside air pressure, ship speed and - if within planets gravity - surface and sea level elevation.
[*][i]autopilot[/i]: Displays the autopilots status. See Autopilot for details.
[*][i]thrust[/i]: Displays current thrust in relation to max effective and max thrust
[*][i]thrustweight[/i]: Display thrust in Newtons for each direction. When within gravity shows thrust to weight ratio for each direction. Useful for building ships.
[*][i]rooms[/i]: Lists all detected rooms and their oxygen levels (if vents are )
[*][i]airlocks[/i]: Lists all airlocks with no additional information atm. See Airlocks for details.
[*][i]hangars[/i]: Lists all defined hangars, their status (open, opening, closing, closed) and the current oxygen level. See Hangar for details.
[*][i]inventory[/i]: Lists inventory fill level, volume and mass.
[*][i]ores[/i]: Lists amount of ores in ship inventory
[*][i]ingots[/i]: Lists amount of ingots in ship inventory
[*][i]connectors[/i]: Lists all connectors and whether there is a vehicle connected. Prints the name of the connected grid.
[*][i]settings[/i]: Displays a list of settings and stored data. Useful for debugging mainly. The executing programmable block will display this information per default.
[/list]

On cockpit LCDs use [i]display#:TYPE[/i] where [i]#[/i] is the index of the display (starting with 0).
To update displays onlys you can run the script with argument [i]init displays[/i]. This is neccessary whenever you add a new display or change the type of an existing display.
All displays will be automatically set to the monospace font and necessary font size.
This script will not adapt to manually set font sizes.

[h1]Airlocks[/h1]
On any [b]sliding door[/b] add [i]airlock:NAME[/i] where [i]NAME[/i] can be any string. Doors will be grouped by this name to an airlock. When one of the doors in an airlock is open, the other doors are deactivated until the opened door is closed again. It will also automatically close any door after a defined delay (See [i]airlock_delay[/i] in Script settings).

Additional parameters:
[i]room:ROOM_NAME[/i]: Defines the room the door leads to. If set, the script only blocks doors to unsafe rooms. This is handy, because it reduces waiting time in airlocks. 
Doors without the room parameter are treated like they face outside and will therefore be locked according to the outside pressure (see Air Vents).

Note: Room names must correspond with Air Vents. If the given room has no air vent, the script assumes 0 air pressure and blocks the door.

[h1]Air Vents[/h1]
On any [b]air vent[/b], add [i]room:ROOM_NAME[/i], where [i]ROOM_NAME[/i] is any string, to add the air vent to a room. 
Having air vents associated with rooms is neccessary for things like displaying the air pressure in rooms, automatically pressurize and depressurize hangars and the airlocks working more smoothly.

Add one outside air vent with the property [i]vent:environment[/i] and no room property to get additional support for airlocks and hangars.

[h1]Lights[/h1]
On any light add [i]room:ROOM_NAME[/i] to associate the light to the given room.
This allows to toggle on/off all lights in a room:

[u][b]Toggling Lights[/b][/u]
You can control lights by running the programmable block with the following commands:
[list]
[*][i]lights on ROOM_NAME[/i]
[*][i]lights off ROOM_NAME[/i]
[*][i]lights toggle ROOM_NAME[/i]
[/list]

[h1]Hangar[/h1]
You can set up automated hangars (depressurizing before opening, repressurizing after, warning lights, etc). To do this, add [i]hangar:ROOM_NAME[/i] to all hangar doors.
For the automated depressurizing to work, you will also need at least one vent in this room (see [Air Vents](#air-vents) for details how to set them up).

[u][b]Warning lights[/b][/u]
It is possible to add [i]hangar:ROOM_NAME[/i] to any lights (recommended lights are the rotary ones). These will be used as warning lights (do not put [i]room:ROOM_NAME[/i] on them). 
They will be toggled on while opening and closing, and off when the hangar is open or closed. Set Color, blink rate, etc manually.

[u][b]Status LCDs[/b][/u]
It is also possible to add status LCDs (currently only corner LCDs are really supported) by adding [i]hangar:ROOM_NAME[/i] (Make sure they DO NOT have an [i]display[/i] property as well). 
If set up, they will switch between a green "Hangar pressurized", a red "Hangar depressurized" or a yellow/orange "Hangar pressurizing/depressurizing", depending on the status. 

[u][b]Opening/Closing the Hangar[/b][/u]
Opening and closing the hangar can be triggered via the programmable block with the following arguments:
[i]hangar open ROOM_NAME[/i] or [i]hangar close ROOM_NAME[/i]

You can open/close all hangars at once with [i]hangar open all[/i] or [i]hangar close all[/i]

[h1]Bugs, Suggestions, Questions and Disclaimer[/h1]
I will likely continue to work on the script for a while, but I won't guarantee any active development or support. The script is provided as is.
That being said, I do have the sript in a public [url=https://gitlab.com/Fitzi/operator-another-space-engineers-automation-script]GitLab repository[/url], so feel free to open issues for bugs, feature ideas, questions or if you happen to be a developer yourself, drop a merge request with improvements or new features there.

The script is released under the [url=https://choosealicense.com/licenses/gpl-3.0]GNU General Public License v3.0[/url].