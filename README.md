﻿# OPERATOR - anOther sPace EngineeRs AuTomatiOn scRipt

OPERATOR combines a bunch of useful features, like displaying information around resources (hydrogen, oxygen, batteries, etc), your ships status, etc.
It also adds some useful automations like self-closing/locking airlocks and hangar doors.

## General

All the scripts functionality is controlled via CustomData properties.
You do not need to edit the script itself.

It will only consider blocks in the same grid as the programmable block itself.

## Script settings

The script settings are defined in the programmable blocks CustomData.

 The format is `KEY:VALUE` (one per line) where KEY is one of the following values:

- `mode`: Determines the mode the script runs in. Possible values are `Main`, `Miner`, `Fighter`. The `Main` mode will include all features, 
while `Miner` and `Fighter` will only run a reduced featureset more tailored to small miner or fighter ships. Defaults to `Main`. (Note: modes are case sensitive)
- `speed`: Sets the ships intended max speed. Currently only used to display a speed bar on environment displays. Defaults to 100.
- `airlock_delay`: Sets the number of ticks a doors in an airlock stays open. Defaults to 20. (Must be multiple of 10; 1 second = 60 ticks)

After changing any settings, you need to run the programmable block with arugment `init` to load the changes.

### Ship Modes

The ship mode will determine the features available. This is useful to safe some processing power, by not running everything on every ship.

In `Fighter` mode, only these features are available

- Display Batteries
- Display Environment

In `Miner` mode, there are additionally

- Display Inventories
- Display Reactor
- Display Energy
- Display Thrust
- Display Connectors
- Display Hydrogen
- Display Oxygen
- Display Solar
- Autopilot functionality

In `Main` mode, there are additionally
- Display Rooms
- Lights functionality
- Airlocks
- Air Vents
- Hangars

## Updating the script

The script only searches blocks and processes custom data on startup, to keep cpu usage low. 
Whenever you add or remove blocks or change custom data, the script needs to be reinitialized,

by running the programmable block with the argument `init`.

In case you only changed your displays, you can run `init displays` to only reload displays.

To easily find/configure displays, you can run `displays:find`. It will walk over all diplays and looks for display that have PublicData that starts with `display:`.
The display type will be copied to the CustomData, the PublicData will be cleared. If it has a `name:<something>` attribute, it the LCD block itself will be renamed.

## Display information on LCDs

On any **LCD** (*), you can display any of the listed information by adding `display:TYPE` to the displays custom data, where `TYPE` can be any of the following values:

- `hydrogen`: Displays total capacity, available capacity, current fill level, stockpile warnings, etc.
- `oxygen`: Same info as hydrogen, just for oxygen

- `energy`: Summarizes energy production. Will show current output percentage of batteries, solar, reactors and other. Also shows warning for low uranium levels.
- `batteries`: Displays detailed information for batteries like current charge level, max capacity, current in/output, mode warnings, etc.
- `solar`:  Displays detailed information like current output, max possible output (depending on the sun angle) and max total output
- `reactor`: Displays detailed information for reactors like current and max reactor output and uranium info.

- `environment`: Displays natural gravity, outside air pressure, ship speed and - if within planets gravity - surface and sea level elevation.
- `autopilot`: Displays the autopilots status.  See [Autopilot](#autopilot) for details.
- `thrust`: Displays current thrust in relation to max effective and max thrust
- `thrustweight`: Display thrust in Newtons for each direction. When within gravity shows thrust to weight ratio for each direction. Useful for building ships. Will also show Requires Thrust per Direction, depening on the ortientation and a warning if limit is near. Usefull for drill or cargo ships.

- `jumpdrives`: Displays charging status of jump drive(s)

- `rooms`: Lists all detected rooms and their oxygen levels (if vents are )
- `airlocks`: Lists all airlocks with no additional information atm.  See [Airlocks](#airlocks) for details.
- `hangars`: Lists all defined hangars, their status (open, opening, closing, closed) and the current oxygen level. See [Hangar](#hangar) for details.

- `inventory`: Lists inventory fill level, volume and mass.
- `ores`: Lists amount of ores in ship inventory
- `ingots`: Lists amount of ingots in ship inventory

- `statusreport`: Will show a summary of all warnings. Hydrogen/Oxygen stockpile warnings, batteries in recharge mode, low battery charge, low uranium, thrust limits reached, hangars opening/closing, jump drives active

- `connectors`: Lists all connectors and whether there is a vehicle connected. Prints the name of the connected grid.
- `settings`: Displays a list of settings and stored data. Useful for debugging mainly. The executing programmable block will display this information per default.

On cockpit LCDs use `display#:TYPE` where `#` is the index of the display (starting with 0) e.g. `display0:batteries`.

To update displays onlys you can run the script with argument `init displays`. 
This is neccessary whenever you add a new display or change the type of an existing display.

All displays will be automatically set to the monospace font and necessary font size.
This script will not adapt to manually set font sizes.

\* Fully supported LCDS are:
- Standard, Transparent and Wide LCDs (on small and large grids)
- Cockpit and Industrial Cockpit LCDs
- Control Seat LCD

Other displays should generally work but might crop some content.

## Airlocks

On any **sliding door** add `airlock:NAME` where `NAME` can be any string. Doors will be grouped by this name to an airlock. When one of the doors in an airlock is open, the other doors are deactivated until the opened door is closed again. It will also automatically close any door after a defined delay (See `airlock_delay` in [Script settings](#script-settings)).

Additional parameters:
- `room:ROOM_NAME`: Defines the room the door leads to. If set, the script only blocks doors to unsafe rooms. This is handy, because it reduces waiting time in airlocks. 
Doors without the room parameter are treated like they face outside and will therefore be locked according to the outside pressure (if set up, see Environmental Air Pressure in [Air Vents](#air-vents)).

*Note*: Room names must correspond with [Air Vents](#air-vents). If the given room has no air vent, the script assumes 0 air pressure and blocks the door.

## Air Vents

On any **air vent**, add `room:ROOM_NAME`, where `ROOM_NAME` is any string, to add the air vent to a room. 
Having air vents associated with rooms is neccessary for things like displaying the air pressure in rooms, automatically pressurize and depressurize hangars and the airlocks working more smoothly.

### Environmental Air Pressure

Add one outside air vent with the property `vent:environment` and no room property to get additional support for airlocks and hangars.

## Lights

On any light add `room:ROOM_NAME` to associate the light to the given room.
This allows to toggle on/off all lights in a room:

### Toggling Lights

You can control lights by running the programmable block with the following commands:
- `lights on ROOM_NAME`
- `lights off ROOM_NAME`
- `lights toggle ROOM_NAME`

## Hangar

You can set up automated hangars (depressurizing before opening, repressurizing after, warning lights, etc). To do this, add `hangar:ROOM_NAME` to all hangar doors.
For the automated depressurizing to work, you will also need at least one vent in this room (see [Air Vents](#air-vents) for details how to set them up).

*Note*: Hangar doors will close when you run the script for the very first time on a programmable block. Make sure to move things out of the way.
After the first run, the state gets persisted and is kept after recompiling and even after restarting the game.

*Note2*: Do not use `all` as `ROOM_NAME`.

### Warning lights

It is possible to add `hangar:ROOM_NAME` to any lights (recommended lights are the rotary ones). These will be used as warning lights (do not put `room:ROOM_NAME` on them, or they will also be used as normal lights). 
They will be toggled on in "warning situations" (while opening and closing), and off while the hangar is open or closed. Color, blink rate, etc must be manually set to whatever you see fit.

### Warning/Status LCDs

It is also possible to add status LCDs (currently only corner LCDs are really supported) by adding `hangar:ROOM_NAME` (Make sure they DO NOT have an `display` property as well). 
If set up, they will switch between a green "Hangar pressurized", a red "Hangar depressurized" or a yellow/orange "Hangar pressurizing/depressurizing", depending on the status. 
Useful to display the current status on the other side of the airlock to a hangar.

### Opening/Closing the Hangar

Opening and closing the hangar can be triggered via the programmable block with the following arguments:
- `hangar open ROOM_NAME` 
- `hangar close ROOM_NAME`

You can also operate all hangars at once with
- `hangar open all` or `hangar close all`

Simplest way is to set up a button that runs the programmable block with the appropriate argument.

## Autopilot

Not a real autopilot in the sense of bringing you and your ship somewhere! Just a set of usefull, small features that can be helpful in survival.

- Enables inertia dampeners when the ships is in or enters gravity and is uncontrolled.
- Disables thrusters and put batteries in recharge mode when ship is docked to a connector and the cockpit is left. (in `Miner` mode only)
  Will reenable thrusters and put batteries back to auto when cockpit is entered again.

To enable or disable auto on/off when docked run `autopilot auto_off enable` or `autopilot auto_off disable`.

To enable or disable the autopilot entirey, run `autopilot enable` or `autopilot disable`.

## Command overview

The programmable block can be executed with the following arguments (e.g. by adding the programmable block RUN option to a button):

- `hangar open/close ROOM_NAME` to trigger the opening/closing procedure (de/pressurizing the room, opening/closing the hangar doors, *all* is a special room name)
- `lights on/off/toggle ROOM_NAME` to turn lights on/off or toggle between states in a given room
- `autopilot enable/disable` to turn on/off the entire autopilot feature
- `autopilot auto_off enable/disable` to turn of the auto on/off feature only, where ships in miner mode put batteries to recharge on thruster of when docked and left.
- `init displays` to reinitialize displays. Neccessary for CustomData changes to take effect.
- `init` to reinitialize the entire script. Updates all cached blocks and displays.
- `prefix` to prefix all Blocks in the same Grid as the programmable block with the same [CODE] as the Grid Name (in Info Tab). E.g. Grid Name is "[MOAB] Massive Ornary Air Blimp" every blocks name in the grid will be prefixed with "[MOAB]". This is helpful if you need to know which block belongs to which grid in the control panel, if you have a lot of ships docked.

## Script minification

The script in its "raw" form exceeds the character limit. To solve this I added a (very crude) minifier in form of `minify.js`. Run with `node minify.js` to generate a minified version.  
Run with `--watch` to start a file watcher, that automatically minifies on change. The minifier will also copy the new code to the clipboard, so you don't need to open the file and copy the contents manually.  

## Disclaimer, License and Contributions

This project originated in a small script to auto-depressurize my hangar and add simple airlocks to my survival game. It grew quiet a bit over the last few years,
mainly because I often found it more interesting to automate the game than to actually play it. I think it is a nice - easy to use - script that brings some useful 
features into vanilla survival games, that's why I decided to publish it ~in the workshop~ (the workshop does contain an older version only).

I will likely continue to work on the script on and off, but I won't guarantee any active development or support. The script is provided as is.
That being said, the script is in a public [GitLab repository](https://gitlab.com/Fitzi/operator-another-space-engineers-automation-script), so feel free to open issues for bugs, improvements or questions. 
If you happen to be a developer yourself, feel free to drop a merge request with improvements or new features.

Also feel free to fork the project and change it to your likings, it's released under the [GNU General Public License v3.0](https://choosealicense.com/licenses/gpl-3.0/).
