using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using VRageMath;
using VRage.Game;
using VRage.Collections;
using Sandbox.ModAPI.Ingame;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using Sandbox.Game.EntityComponents;
using SpaceEngineers.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Library.Utils;

namespace SpaceEngineersScripts.MasterScript
{
    partial class Program : MyGridProgram 
    {

        #region script

        // standard large grid square LCDs
        private const float HANGAR_FONT_SIZE = 8.0f;
        private const string FONT = "Monospace";
        private const float MIN_PRESSURE_LEVEL = 0.75f;

        // global timer
        private TimeSpan Uptime;

        public Program()
        {
            // Update every 10 and 100 ticks
            Runtime.UpdateFrequency = UpdateFrequency.Update10 | UpdateFrequency.Update100;
            Uptime = TimeSpan.Zero;

            LoadProgrammSettings();
            InitAll();
        }

        #region programm_settings
        private enum ProgramMode { Main, Miner, Fighter };

        private ProgramMode SelectedProgramMode = ProgramMode.Main;
        private double ShipMaxSpeed = 100;
        private int AirlockAutoClosingDelay = 20; // ticks

        private void LoadProgrammSettings()
        {
            var properties = GetKeyValuePairsFromString(Me.CustomData);

            if (properties.ContainsKey("mode"))
                Enum.TryParse<ProgramMode>(properties["mode"], out SelectedProgramMode);

            Echo("Programm runs in " + SelectedProgramMode.ToString() + " mode");

            if (properties.ContainsKey("speed"))
                double.TryParse(properties["speed"], out ShipMaxSpeed);

            if (properties.ContainsKey("airlock_delay"))
                int.TryParse(properties["airlock_delay"], out AirlockAutoClosingDelay);
        }
        #endregion
        ///////////////////////////////
        #region entrypoints
        public void Main(string argument, UpdateType updateType)
        {
            this.Uptime += Runtime.TimeSinceLastRun;

            // Update every 10 ticks
            if ((updateType & UpdateType.Update10) != 0)
            {
                Run10();
            }

            // Update every 100 ticks
            if ((updateType & UpdateType.Update100) != 0)
            {
                Run100();
            }

            // Called by button, timer, sensor or other trigger
            if ((updateType & UpdateType.Trigger) != 0)
            {
                ExecuteCommand(argument);
            }

            // Called by an antenna
            if ((updateType & UpdateType.IGC) != 0)
            {
                Echo("IGC not supported yet");
            }

            // Called from another programmable block
            if ((updateType & UpdateType.Script) != 0)
            {
                Echo("Script not supported yet");
            }

            // Called manually through the Terminal
            if ((updateType & UpdateType.Terminal) != 0)
            {
                ExecuteCommand(argument);
            }
        }

        public void Save()
        {
            SaveSettings();
        }

        private void ExecuteCommand(string argument)
        {
            if (argument == "init")
            {
                LoadProgrammSettings();
                InitAll();
            }
            else if (argument == "init displays")
            {
                InitDisplays();
            }
            else if (argument.StartsWith("hangar"))
            {
                CommandHangar(argument.Substring(6).TrimStart());
            }
            else if (argument.StartsWith("lights"))
            {
                CommandLights(argument.Substring(6).TrimStart());
            }
            else if (argument.StartsWith("autopilot"))
            {
                CommandAutopilot(argument.Substring(9).TrimStart());
            }
            else if( argument == "prefix" ){
                PrefixBlocksWithShortCode();
            }
            else if( argument == "displays:find" ){
                FindDisplaysByPublicData();
            }
            else
            {
                Echo("Command not supported");
            }
        }

        private void InitAll()
        {
            LoadSettings();

            InitShipReference();
            InitDisplays();
            InitBatteries();
            InitEnvironment();

            if (SelectedProgramMode != ProgramMode.Fighter)
            {
                InitInventories();
                InitReactor();
                InitEnergy();
                InitThrust();
                InitConnectors();
                InitAutopilot();
                InitGasStorage();
                InitSolar();
            }

            if (SelectedProgramMode == ProgramMode.Main)
            {
                InitRooms();
                InitLights();
                InitAirlocks();
                InitHangars();
                InitJumpdrives();
            }
        }

        /**
         * Runs every 10 ticks
         */
        private void Run10()
        {
            UpdateEnvironment10();

            if (SelectedProgramMode == ProgramMode.Main)
            {
                UpdateAirlocks();
            }

            UpdateDisplays10();
        }

        /**
         * Runs every 100 ticks
         */
        private void Run100()
        {
            UpdateBatteries();
            UpdateInventories();
            UpdateEnvironment100();

            if (SelectedProgramMode != ProgramMode.Fighter)
            {
                UpdateReactor();
                UpdateThrust();
                UpdateThrustWeight();
                UpdateConnectors();
                UpdateGasStorage();
                UpdateSolar();
            }

            if (SelectedProgramMode == ProgramMode.Main)
            {
                UpdateRooms();
                UpdateLights();
                UpdateHangars();
                UpdateJumpdrives();
            }

            if (SelectedProgramMode != ProgramMode.Fighter)
            {
                UpdateEnergy(); // depends on batteries, reactor and solar
                UpdateAutopilot();
            }

            // Displays should be last (for obvious reasons)
            UpdateDisplays100();
        }
        #endregion
        ///////////////////////////////
        #region displays
        private enum DisplayMode { hydrogen, oxygen, batteries, solar, energy, thrust, thrustweight, jumpdrives, environment, airlocks, rooms, hangars, inventory, ores, ingots, autopilot, connectors, settings, reactor, checklist, statusreport, debug };
        private Dictionary<DisplayWidths, Dictionary<DisplayMode, List<IMyTextSurface>>> displays = new Dictionary<DisplayWidths, Dictionary<DisplayMode, List<IMyTextSurface>>>();

        // S = square LCDs, M = cockpit and block displays, L = Wide LCD
        private enum DisplayWidths { S = 20, M = 30, L = 40 };
        private DisplayWidths CurrentDisplayWidth = DisplayWidths.S;
        private int DisplayWidth = 20;
        private int DisplayHeight = 13;

        private Color COLOR_ORANGE = new Color(255, 161, 20);
        private Color COLOR_GREEN = new Color(42, 255, 36);
        private Color COLOR_RED = new Color(255, 22, 27);

        //private Color CockpitDisplaysBackgroundColor = new Color(0, 70, 130);

        private void InitDisplays()
        {
            this.displays.Clear();

            this.displays.Add(DisplayWidths.S, new Dictionary<DisplayMode, List<IMyTextSurface>>());
            this.displays.Add(DisplayWidths.M, new Dictionary<DisplayMode, List<IMyTextSurface>>());
            this.displays.Add(DisplayWidths.L, new Dictionary<DisplayMode, List<IMyTextSurface>>());

            // LCDs
            List<IMyTextPanel> textPanels = new List<IMyTextPanel>();
            GridTerminalSystem.GetBlocksOfType<IMyTextPanel>(textPanels, CurrentGridOnly);
            foreach (var display in textPanels)
            {
                var properties = GetKeyValuePairsFromString(display.CustomData);
                if (!properties.ContainsKey("display"))
                    continue;

                bool isTransparent = ((IMyTextSurface)display).Name.StartsWith("Transparent");
                float fontSize = isTransparent ? 1.32f : 1.25f;
                float padding = isTransparent ? 0f : 2f;

                bool isWide = ((IMyTextSurface)display).SurfaceSize.X == 1024;

                AddTextSurface(properties["display"], isWide ? DisplayWidths.L : DisplayWidths.S, display, fontSize, padding);
            }

            // Cockpits
            List<IMyCockpit> cockpits = new List<IMyCockpit>();
            GridTerminalSystem.GetBlocksOfType<IMyCockpit>(cockpits, CurrentGridOnly);
            foreach (var cockpit in cockpits)
            {
                var properties = GetKeyValuePairsFromString(cockpit.CustomData);

                for (int i = 0; i < cockpit.SurfaceCount; i++)
                {
                    string key = "display" + (cockpit.SurfaceCount > 1 ? i.ToString() : "");

                    if (!properties.ContainsKey(key))
                        continue;

                    // TODO differentiate between cockpits and other displays and set font size and padding accordingly
                    // bool isLargeGrid = cockpit.CubeGrid.GridSizeEnum == VRage.Game.MyCubeSize.Large;
                    bool isActualCockpit = cockpit.SurfaceCount > 2; // all cockpits have more than 2 text surfaces

                    float fontSize = isActualCockpit ? 0.84f : 0.865f;
                    float padding = isActualCockpit ? 2f : 0.5f;

                    AddTextSurface(properties[key], DisplayWidths.M, cockpit.GetSurface(i), fontSize, padding);

                    //if (isActualCockpit)
                    //{
                    //    cockpit.GetSurface(i).BackgroundColor = CockpitDisplaysBackgroundColor;
                    //}
                }
            }

            // other surface providers
            List<IMyTerminalBlock> providers = new List<IMyTerminalBlock>();
            GridTerminalSystem.GetBlocksOfType<IMyTextSurfaceProvider>(providers, CurrentGridOnly);
            foreach (var provider in providers)
            {
                // ignore lcds and cockpits
                if (provider is IMyTextPanel || provider is IMyCockpit || provider == Me)
                    continue;

                var properties = GetKeyValuePairsFromString(provider.CustomData);

                int count = ((IMyTextSurfaceProvider)provider).SurfaceCount;

                for (int i = 0; i < count; i++)
                {
                    string key = "display" + (count > 1 ? i.ToString() : "");

                    if (!properties.ContainsKey(key))
                        continue;

                    var surface = ((IMyTextSurfaceProvider)provider).GetSurface(i);

                    DisplayWidths displayWidth = DisplayWidths.M;
                    float fontSize = 0.865f;
                    float padding = 0.5f;

                    if ((count == 1 && surface.SurfaceSize.X == 256) || (count == 4 && surface.SurfaceSize.X == 128))
                    {
                        // Sparks of the Future Panel + Button or 4 Buttons Panel
                        displayWidth = DisplayWidths.S;
                        fontSize = 1.265f;
                        padding = 2f;
                    }

                    AddTextSurface(properties[key], displayWidth, surface, fontSize, padding);
                }
            }

            // Programmable block itself
            AddTextSurface(DisplayMode.settings.ToString(), DisplayWidths.M, Me.GetSurface(0), 0.86f, 0.5f);

        }

        private void FindDisplaysByPublicData(){
            int found = 0;

            List<IMyTextPanel> textPanels = new List<IMyTextPanel>();
            GridTerminalSystem.GetBlocksOfType<IMyTextPanel>(textPanels, CurrentGridOnly);
            foreach (var display in textPanels)
            {
                var properties = GetKeyValuePairsFromString(display.GetText());
                if (!properties.ContainsKey("display"))
                    continue;
                
                found++;

                if( properties.ContainsKey("name") )
                    display.CustomName = properties["name"];

                display.CustomData = "display:" + properties["display"];
                display.WriteText("", false);
            }

            Echo($"Found {found}/{textPanels.Count} displays");
        }

        private void AddTextSurface(string modeString, DisplayWidths width, IMyTextSurface surface, float fontSize, float padding)
        {
            DisplayMode mode;
            if (!DisplayMode.TryParse(modeString, out mode))
                return;

            if (!this.displays[width].ContainsKey(mode))
            {
                this.displays[width].Add(mode, new List<IMyTextSurface>());
            }

            this.displays[width][mode].Add(surface);

            surface.Font = FONT;
            surface.ContentType = ContentType.TEXT_AND_IMAGE;
            surface.FontSize = fontSize;
            surface.TextPadding = padding;
        }

        private bool HasDisplaysForMode(DisplayMode mode, int page = 1)
        {
            return displays[CurrentDisplayWidth].ContainsKey(mode);
        }

        private List<IMyTextSurface> GetDisplaysForMode(DisplayMode mode)
        {
            return displays[CurrentDisplayWidth][mode];
        }

        private void SetCurrentDisplayWidth(DisplayWidths width)
        {
            CurrentDisplayWidth = width;
            DisplayWidth = (int)width;
        }

        private void UpdateDisplays10()
        {
            foreach (DisplayWidths width in Enum.GetValues(typeof(DisplayWidths)))
            {
                SetCurrentDisplayWidth(width);
                DisplayEnvironment();
                DisplayStatusReport();
            }
        }

        private void UpdateDisplays100()
        {
            foreach (DisplayWidths width in Enum.GetValues(typeof(DisplayWidths)))
            {
                SetCurrentDisplayWidth(width);

                // all
                DisplayBatteries();
                DisplaySettings();
                DisplayDebug();

                if (SelectedProgramMode != ProgramMode.Fighter)
                {
                    DisplayInventory();
                    DisplayReactor();
                    DisplayEnergy();
                    DisplayThrust();
                    DisplayThrustWeight();
                    DisplayAutopilot();
                    DisplayGasStorage();
                    DisplaySolar();
                }

                if (SelectedProgramMode == ProgramMode.Main)
                {
                    DisplayAirlocks();
                    DisplayRooms();
                    DisplayHangars();
                    DisplayConnectors();
                    DisplayJumpdrives();
                }
            }
        }
        #endregion
        ///////////////////////////////
        #region settings
        private Dictionary<string, bool> Settings = new Dictionary<string, bool>();

        private void SaveSettings()
        {
            var builder = new StringBuilder();
            foreach (var el in Settings)
            {
                builder.Append(el.Key);
                builder.Append(':');
                builder.Append(el.Value ? "1" : "0");
                builder.Append(';');
            }

            if (builder.Length > 0)
                builder.Length--; // remove trailing ;

            Storage = builder.ToString();
        }

        private void LoadSettings()
        {
            Settings.Clear();

            string[] storedData = Storage.Split(';');
            foreach (var element in storedData)
            {
                string[] keyValuePair = element.Split(':');
                if (keyValuePair.Length == 2)
                {
                    Settings.Add(keyValuePair[0], keyValuePair[1] == "1");
                }
            }
        }

        private bool HasSetting(string key)
        {
            return Settings.ContainsKey(key);
        }

        private void SetSetting(string key, bool value)
        {
            if (HasSetting(key))
            {
                Settings[key] = value;
            }
            else
            {
                Settings.Add(key, value);
            }
        }

        private void ClearSetting(string key)
        {
            if (HasSetting(key))
                Settings.Remove(key);
        }

        private bool GetSetting(string key, bool fallback = false)
        {
            if (HasSetting(key))
                return Settings[key];
            return fallback;
        }

        private void DisplaySettings()
        {
            if (!HasDisplaysForMode(DisplayMode.settings)) return;

            var text = GetStringBuilderForScreen();
            CreateHeader(text, "Settings");

            CreateTwoColumnText(text, "Uptime", Uptime.Hours.ToString("00") + ":" + Uptime.Minutes.ToString("00") + ":" + Uptime.Seconds.ToString("00"));

            text.Append('\n');

            CreateTwoColumnText(text, "Mode", SelectedProgramMode.ToString());
            CreateTwoColumnText(text, "Max Speed", ShipMaxSpeed.ToString(ThousandSeparatorNumberFormat));
            CreateTwoColumnText(text, "Airlock Delay", AirlockAutoClosingDelay.ToString());

            text.Append('\n');

            text.Append("Stored data:\n");
            foreach (var element in Settings)
            {
                CreateTwoColumnText(text, element.Key, element.Value ? "yes" : "no");
            }

            WriteRenderedTextToScreens(DisplayMode.settings, text);
        }

        #endregion
        ///////////////////////////////
        #region helpers
        private const string ThousandSeparatorNumberFormat = "#,##0";
        private const string ThousandSeparatorNumberFormatWithSingleDecimalPlace = "#,##0.0";
        private const string DoubleDecimalPlacesFormat = "0.00";
        private const string PercentageNumberFormat = "P0";

        #region characters
        public const char LINE_VERT = '\u2502';
        public const char LINE_HOR = '\u2500';

        public const char CORNER_TOP_LEFT = '\u250c';
        public const char CORNER_TOP_RIGHT = '\u2510';
        public const char CORNER_BOTTOM_LEFT = '\u2514';
        public const char CORNER_BOTTOM_RIGHT = '\u2518';

        public const char HALF_BLOCK_TOP = '\u2580';
        public const char HALF_BLOCK_BOTTOM = '\u2584';
        public const char HALF_BLOCK_LEFT = '\u258c';
        public const char HALF_BLOCK_RIGHT = '\u2590';

        public const char QUARTER_BLOCK_BOTTOM = '\u2581';

        public const char BLOCK_FULL = '\u2588';
        public const char BLOCK_LIGHT = '\u2591';
        public const char BLOCK_MEDIUM = '\u2592';
        public const char BLOCK_DENSE = '\u2593';

        public const char CHECKMARK = '\u2713';

        public const char SUP_TWO = '\u00b2';
        public const char SUP_THREE = '\u00b3';
        public string QM = "m" + SUP_THREE;
        #endregion
        
        private Dictionary<string, string> GetKeyValuePairsFromString(string customData)
        {
            var lines = new List<string>(customData.Split('\n'));
            var properties = new Dictionary<string, string>();
            foreach (var line in lines)
            {
                var pair = line.Split(':');
                if (pair.Length == 2)
                {
                    properties.Add(pair[0], pair[1]);
                }
            }
            return properties;
        }

        private bool CurrentGridOnly(IMyCubeBlock block)
        {
            return block.CubeGrid == Me.CubeGrid;
        }

        #region Screen
        private StringBuilder GetStringBuilderForScreen()
        {
            // + 1 to account for line breaks
            return new StringBuilder((DisplayWidth + 1) * DisplayHeight);
        }

        #region Header
        private void CreateHeader(StringBuilder text, string title)
        {
            text.Append(title);
            text.Append('\n');
            text.Append(LINE_HOR, DisplayWidth);
            text.Append('\n');
        }
        private void CreateTwoColumnHeader(StringBuilder text, string title, string right)
        {
            CreateTwoColumnText(text, title, right);
            text.Append(LINE_HOR, DisplayWidth);
            text.Append('\n');
        }
        #endregion

        #region TextAlignment
        private void CreateCenteredText(StringBuilder text, string textCentered, char spacer = ' ')
        {
            int textLength = textCentered.Length;
            int spaceLeft = (int)Math.Max(0, Math.Floor((DisplayWidth - textLength) / 2d));
            int spaceRight = Math.Max(0, DisplayWidth - spaceLeft - textLength);
            text.Append(spacer, spaceLeft);
            text.Append(textCentered);
            text.Append(spacer, spaceRight);
            text.Append('\n');
        }
        #endregion

        #region MulticolumnText
        private void CreateTwoColumnText(StringBuilder text, string textLeft, string textRight, char spacer = ' ')
        {
            int lenLeft = textLeft.Length;
            int lenRight = textRight.Length;
            int spaceBetween = DisplayWidth - lenLeft - lenRight;

            text.Append(textLeft);
            if (spaceBetween > 0)
            {
                text.Append(spacer, spaceBetween);
            }
            text.Append(textRight);
            text.Append('\n');
        }
        #endregion

        #region TextBar
        private void CreateBar(StringBuilder text, double percentage, bool border = true)
        {
            CreateBar(text, percentage, 0, border);
        }

        private void CreateBar(StringBuilder text, double percentage1, double percentage2, bool border = true, bool upDownMode = false)
        {
            int barWidth = DisplayWidth - 2;

            int width1 = Math.Max(0, (int)(barWidth * percentage1));
            int width2 = Math.Max(0, (int)(barWidth * percentage2));


            if (border)
            {
                text.Append(CORNER_TOP_LEFT);
                text.Append(LINE_HOR, barWidth);
                text.Append(CORNER_TOP_RIGHT);
                text.Append('\n');
            }

            text.Append(LINE_VERT);

            if (upDownMode)
            {
                int remainingWidth = Math.Max(0, barWidth - Math.Max(width1, width2));

                text.Append(BLOCK_FULL, Math.Min(width1, width2));
                text.Append(width1 > width2 ? HALF_BLOCK_TOP : HALF_BLOCK_BOTTOM, Math.Max(width1, width2) - Math.Min(width1, width2));
                text.Append(' ', remainingWidth);
            }
            else
            {
                width2 = Math.Max(0, width2 - width1);
                int remainingWidth = Math.Max(0, barWidth - width1 - width2);

                text.Append(BLOCK_FULL, width1);
                text.Append(HALF_BLOCK_BOTTOM, width2);
                text.Append(border ? ' ' : QUARTER_BLOCK_BOTTOM, remainingWidth);
            }

            text.Append(LINE_VERT);

            if (border)
            {
                text.Append('\n');
                text.Append(CORNER_BOTTOM_LEFT);
                text.Append(LINE_HOR, barWidth);
                text.Append(CORNER_BOTTOM_RIGHT);
            }

            text.Append('\n');
        }

        private void CreateDoubleBar(StringBuilder text, double leftPercentage, double rightPercentage)
        {
            int barWidth = DisplayWidth - 2;

            int leftWidth = Math.Min(barWidth, Math.Max(0, (int)(barWidth * leftPercentage)));
            int rightWidth = Math.Min(barWidth, Math.Max(0, (int)(barWidth * rightPercentage)));

            int inBetweenWidth = barWidth - (leftWidth + rightWidth);

            text.Append(LINE_VERT);

            // overlapping
            if (inBetweenWidth < 0)
            {
                text.Append(HALF_BLOCK_BOTTOM, barWidth - rightWidth);
                text.Append(BLOCK_FULL, Math.Abs(inBetweenWidth));
                text.Append(HALF_BLOCK_TOP, barWidth - leftWidth);
            }
            else
            {
                text.Append(HALF_BLOCK_BOTTOM, leftWidth);
                text.Append(' ', inBetweenWidth);
                text.Append(HALF_BLOCK_TOP, rightWidth);
            }

            text.Append(LINE_VERT);
            text.Append('\n');
        }

        #endregion

        private void CreateChecklistItem(StringBuilder text, string item, bool isChecked)
        {
            text.Append('[');
            text.Append(isChecked ? CHECKMARK : ' ');
            text.Append("] ");
            text.Append(item);
            text.Append('\n');
        }

        private void WriteRenderedTextToScreens(DisplayMode mode, StringBuilder text)
        {
            var panels = GetDisplaysForMode(mode);

            string renderedText = text.ToString();

            foreach (var panel in panels)
            {
                panel.WriteText(renderedText);
            }
        }

        #endregion

        #endregion
        ///////////////////////////////
        #region gas_storage
        private enum GasType { Hydrogen, Oxygen };

        private Dictionary<GasType, List<IMyGasTank>> gasStorageTanks = new Dictionary<GasType, List<IMyGasTank>>();
        private Dictionary<GasType, double> gasStorageTotalCapacityInQm = new Dictionary<GasType, double>();

        private Dictionary<GasType, double> gasStorageTotalFillPercentage = new Dictionary<GasType, double>();
        private Dictionary<GasType, double> gasStorageAvailableFillPercentage = new Dictionary<GasType, double>();
        private Dictionary<GasType, double> gasStorageAvailableTotalCapacityInQm = new Dictionary<GasType, double>();
        private Dictionary<GasType, double> gasStorageNumInStockpileMode = new Dictionary<GasType, double>();

        private Dictionary<GasType, double> gasStorageLastTotalFillPercentage = new Dictionary<GasType, double>();

        private void InitGasStorage()
        {
            gasStorageTanks.Clear();
            gasStorageTotalCapacityInQm.Clear();

            gasStorageTotalFillPercentage.Clear();
            gasStorageAvailableFillPercentage.Clear();
            gasStorageAvailableTotalCapacityInQm.Clear();
            gasStorageNumInStockpileMode.Clear();

            gasStorageLastTotalFillPercentage.Clear();

            foreach ( GasType type in Enum.GetValues(typeof(GasType)))
            {
                gasStorageTanks.Add(type, new List<IMyGasTank>());
                gasStorageTotalCapacityInQm.Add(type, 0);
                gasStorageTotalFillPercentage.Add(type, 0);
                gasStorageAvailableFillPercentage.Add(type, 0);
                gasStorageAvailableTotalCapacityInQm.Add(type, 0);
                gasStorageNumInStockpileMode.Add(type, 0);

                gasStorageLastTotalFillPercentage.Add(type, 0);
            }

            List<IMyGasTank> tanks = new List<IMyGasTank>();
            GridTerminalSystem.GetBlocksOfType<IMyGasTank>(tanks, CurrentGridOnly);

            foreach (var tank in tanks)
            {
                GasType type = tank.BlockDefinition.SubtypeId.Contains("Hydro") ? GasType.Hydrogen : GasType.Oxygen;

                gasStorageTanks[type].Add(tank);
                gasStorageTotalCapacityInQm[type] += tank.Capacity;
            }

            foreach (GasType type in Enum.GetValues(typeof(GasType)))
            {
                gasStorageTotalCapacityInQm[type] /= 1000;
            }

        }

        private void UpdateGasStorage()
        {
            foreach (GasType type in Enum.GetValues(typeof(GasType)))
            {
                gasStorageNumInStockpileMode[type] = 0;

                double fillRatio = 0;
                double availableFillRatio = 0;
                double availableTotalCapacity = 0;

                foreach (var tank in gasStorageTanks[type])
                {
                    fillRatio += tank.FilledRatio;
                    
                    if (tank.Stockpile)
                    {
                        gasStorageNumInStockpileMode[type]++;
                    }
                    else
                    {
                        availableFillRatio += tank.FilledRatio;
                        availableTotalCapacity += tank.Capacity;
                    }
                }

                gasStorageLastTotalFillPercentage[type] = gasStorageTotalFillPercentage[type];
                gasStorageTotalFillPercentage[type] = fillRatio / gasStorageTanks[type].Count;
                gasStorageAvailableFillPercentage[type] = availableFillRatio / gasStorageTanks[type].Count;
                gasStorageAvailableTotalCapacityInQm[type] = availableTotalCapacity / 1000;
            }
        }

        private void DisplayGasStorage()
        {
            foreach (GasType type in Enum.GetValues(typeof(GasType)))
            {
                DisplayMode displayMode = type == GasType.Hydrogen ? DisplayMode.hydrogen : DisplayMode.oxygen;
                string header = type == GasType.Hydrogen ? "Hydrogen" : "Oxygen";

                if (!HasDisplaysForMode(displayMode)) continue;

                var text = GetStringBuilderForScreen();
                CreateHeader(text, header);

                CreateTwoColumnText(text, "Available:", gasStorageAvailableFillPercentage[type].ToString(PercentageNumberFormat));
                CreateBar(text, gasStorageAvailableFillPercentage[type], gasStorageTotalFillPercentage[type],  false, false);

                if (gasStorageNumInStockpileMode[type] > 0)
                {

                    CreateTwoColumnText(text, "Total:", gasStorageTotalFillPercentage[type].ToString(PercentageNumberFormat));
                    text.Append("\n");

                    // Show stockpile warning
                    CreateCenteredText(text, "Stockpile warning!");

                    if (gasStorageNumInStockpileMode[type] == gasStorageTanks[type].Count)
                    {
                        CreateCenteredText(text, "All stockpiling!");
                    }
                    else
                    {
                        CreateCenteredText(text, gasStorageNumInStockpileMode[type].ToString() + "/" + gasStorageTanks[type].Count.ToString() + " tanks");
                    }
                }
                text.Append("\n");
        

                // Gas usage
                double delta = (gasStorageTotalFillPercentage[type] - gasStorageLastTotalFillPercentage[type]) * gasStorageTotalCapacityInQm[type];

                if ( Math.Abs(delta) >= 0.1) {
                    double deltaPerSecond = Math.Abs((delta / 100) * 60); // 1 second = 60 ticks, updates are 100 ticks
                    CreateTwoColumnText(text, (delta < 0 ? "Depleting" : "Filling"), deltaPerSecond.ToString(ThousandSeparatorNumberFormatWithSingleDecimalPlace) + QM + "/s");

                    var secondsLeft = gasStorageTotalCapacityInQm[type] / deltaPerSecond;
                    
                    StringBuilder secondsLeftText = new StringBuilder(DisplayWidth);
                    if( secondsLeft > 60 * 60){
                        secondsLeftText.Append("\u203A"); // ›
                        secondsLeftText.Append( (secondsLeft / (60 * 60)).ToString(ThousandSeparatorNumberFormat) );
                        secondsLeftText.Append(" h");
                    }else if ( secondsLeft > 60 ){
                        secondsLeftText.Append("\u203A"); // ›
                        secondsLeftText.Append( (secondsLeft / (60)).ToString(ThousandSeparatorNumberFormat) );
                        secondsLeftText.Append(" min");
                    }else {
                        secondsLeftText.Append( secondsLeft.ToString(ThousandSeparatorNumberFormat) );
                        secondsLeftText.Append(" sec");
                    }

                    CreateTwoColumnText(text, (delta < 0 ? "Empty in" : "Full in"), secondsLeftText.ToString());
                }
                
                WriteRenderedTextToScreens(displayMode, text);
            }
        }

        #endregion
        ///////////////////////////////
        #region batteries
        private List<IMyBatteryBlock> batteries = new List<IMyBatteryBlock>();
        private double batteriesTotalCapacity;
        private double batteriesMaxOutput;
        private double batteriesMaxInput;

        private double batteriesCharge = 0;
        private double batteriesChargePercentage = 0;

        private double batteriesOutput = 0;
        private double batteriesOutputPercentage = 0;

        private double batteriesInput = 0;
        private double batteriesInputPercentage = 0;

        private int batteriesNumModeRecharging = 0;
        private int batteriesNumModeDischarging = 0;
        private int batteriesNumModeAuto = 0;

        private void InitBatteries()
        {
            batteries.Clear();
            batteriesTotalCapacity = 0;
            batteriesMaxOutput = 0;
            batteriesMaxInput = 0;

            GridTerminalSystem.GetBlocksOfType<IMyBatteryBlock>(batteries, CurrentGridOnly);

            foreach (var battery in batteries)
            {
                batteriesTotalCapacity += battery.MaxStoredPower;
                batteriesMaxOutput += battery.MaxOutput;
                batteriesMaxInput += battery.MaxInput;
            }
        }

        private void UpdateBatteries()
        {
            batteriesCharge = 0;
            batteriesOutput = 0;
            batteriesInput = 0;

            batteriesNumModeRecharging = 0;
            batteriesNumModeDischarging = 0;
            batteriesNumModeAuto = 0;

            foreach (var battery in batteries)
            {

                batteriesCharge += battery.CurrentStoredPower;
                batteriesOutput += battery.CurrentOutput;
                batteriesInput += battery.CurrentInput;

                switch (battery.ChargeMode)
                {
                    case ChargeMode.Auto: batteriesNumModeAuto++; break;
                    case ChargeMode.Discharge: batteriesNumModeDischarging++; break;
                    case ChargeMode.Recharge: batteriesNumModeRecharging++; break;
                }

            }

            batteriesChargePercentage = batteriesTotalCapacity == 0 ? 0 : batteriesCharge / batteriesTotalCapacity;
            batteriesInputPercentage = batteriesMaxInput == 0 ? 0 : batteriesInput / batteriesMaxInput;
            batteriesOutputPercentage = batteriesMaxOutput == 0 ? 0 : batteriesOutput / batteriesMaxOutput;
        }

        private void DisplayBatteries()
        {
            if (!HasDisplaysForMode(DisplayMode.batteries)) return;

            var text = GetStringBuilderForScreen();
            CreateHeader(text, "Batteries");

            CreateTwoColumnText(text, "Charge:", batteriesChargePercentage.ToString(PercentageNumberFormat));

            StringBuilder capacity = new StringBuilder(DisplayWidth);
            capacity.Append(batteriesCharge.ToString(ThousandSeparatorNumberFormatWithSingleDecimalPlace));
            capacity.Append('/');
            capacity.Append(batteriesTotalCapacity.ToString(ThousandSeparatorNumberFormatWithSingleDecimalPlace));
            capacity.Append(" MWh");
            CreateTwoColumnText(text, string.Empty, capacity.ToString());

            CreateBar(text, batteriesChargePercentage, false);
            text.Append('\n');

            StringBuilder input = new StringBuilder(DisplayWidth);
            input.Append(batteriesInput.ToString(ThousandSeparatorNumberFormat));
            input.Append('/');
            input.Append(batteriesMaxInput.ToString(ThousandSeparatorNumberFormat));
            input.Append(" MWh");
            CreateTwoColumnText(text, "Input", input.ToString());

            CreateBar(text, batteriesInputPercentage, batteriesOutputPercentage, false, true);

            StringBuilder output = new StringBuilder(DisplayWidth);
            output.Append(batteriesOutput.ToString(ThousandSeparatorNumberFormat));
            output.Append('/');
            output.Append(batteriesMaxOutput.ToString(ThousandSeparatorNumberFormat));
            output.Append(" MWh");
            CreateTwoColumnText(text, "Output", output.ToString());
            text.Append('\n');

            if (batteriesNumModeRecharging == 0 && batteriesNumModeDischarging == 0)
            {
                CreateCenteredText(text, "All in auto mode!");
            }
            else
            {
                text.Append(batteriesNumModeRecharging);
                text.Append(" recharging\n");
                text.Append(batteriesNumModeDischarging);
                text.Append(" discharging"); 
            }

            WriteRenderedTextToScreens(DisplayMode.batteries, text);
        }
        #endregion
        ///////////////////////////////
        #region solar
        private List<IMySolarPanel> solarPanels = new List<IMySolarPanel>();
        private double solarMaxOutput = 0;

        double solarCurrentMaxOutput = 0;
        double solarCurrentMaxOutputPercentage = 0;
        double solarCurrentOutput = 0;
        double solarCurrentOutputPercentage = 0;

        private void InitSolar()
        {
            solarPanels.Clear();
            solarMaxOutput = 0;

            GridTerminalSystem.GetBlocksOfType<IMySolarPanel>(solarPanels, CurrentGridOnly);
            foreach (var solarPanel in solarPanels)
            {
                solarMaxOutput += 0.160d; // large solar panels can output up to 160 kW
            }
        }

        private void UpdateSolar()
        {
            double currentMaxOutput = 0;
            double currentOutput = 0;

            foreach (var solarPanel in solarPanels)
            {
                currentMaxOutput += solarPanel.MaxOutput;
                currentOutput += solarPanel.CurrentOutput;
            }

            solarCurrentMaxOutput = currentMaxOutput;
            solarCurrentOutput = currentOutput;

            solarCurrentMaxOutputPercentage = solarCurrentMaxOutput / solarMaxOutput;
            solarCurrentOutputPercentage = solarCurrentOutput / solarMaxOutput;
        }

        private void DisplaySolar()
        {
            if (!HasDisplaysForMode(DisplayMode.solar)) return;

            var text = GetStringBuilderForScreen();
            CreateHeader(text, "Solar Panels");

            CreateTwoColumnText(text, "Current:", solarCurrentOutputPercentage.ToString(PercentageNumberFormat));

            StringBuilder output = new StringBuilder(DisplayWidth);
            output.Append(solarCurrentOutput.ToString(ThousandSeparatorNumberFormatWithSingleDecimalPlace));
            output.Append('/');
            output.Append(solarCurrentMaxOutput.ToString(ThousandSeparatorNumberFormatWithSingleDecimalPlace));
            output.Append('/');
            output.Append(solarMaxOutput.ToString(ThousandSeparatorNumberFormatWithSingleDecimalPlace));
            output.Append(" MWh");
            CreateTwoColumnText(text, string.Empty, output.ToString());
            text.Append('\n');

            CreateBar(text, solarCurrentOutputPercentage, solarCurrentMaxOutputPercentage, false);

            WriteRenderedTextToScreens(DisplayMode.solar, text);
        }
        #endregion
        ///////////////////////////////
        #region reactor
        private List<IMyReactor> reactors = new List<IMyReactor>();

        private double reactorMaxOutput = 0;
        private double reactorCurrentOutput = 0;
        private double reactorCurrentOutputPercentage = 0;

        private void InitReactor() { }

        private void UpdateReactor()
        {
            reactors.Clear();
            GridTerminalSystem.GetBlocksOfType<IMyReactor>(reactors, CurrentGridOnly);
            UpdateEnergyProducer(reactors, out reactorMaxOutput, out reactorCurrentOutput, out reactorCurrentOutputPercentage);
        }

        private void DisplayReactor()
        {
            if (!HasDisplaysForMode(DisplayMode.reactor)) return;

            var text = GetStringBuilderForScreen();
            CreateHeader(text, "Reactors");

            CreateTwoColumnText(text, "Current:", reactorCurrentOutputPercentage.ToString(PercentageNumberFormat));

            StringBuilder output = new StringBuilder(DisplayWidth);
            output.Append(reactorCurrentOutput.ToString(ThousandSeparatorNumberFormatWithSingleDecimalPlace));
            output.Append('/');
            output.Append(reactorMaxOutput.ToString(ThousandSeparatorNumberFormatWithSingleDecimalPlace));
            output.Append(" MWh");
            CreateTwoColumnText(text, string.Empty, output.ToString());
            text.Append('\n');

            CreateBar(text, reactorCurrentOutputPercentage, false);
            text.Append('\n');

            var amountUranium = GetAmountOfResource(InventoryResourceType.Ingot, InventoryResourceName.Uranium);
            CreateTwoColumnText(text, "Uranium", amountUranium.ToString(ThousandSeparatorNumberFormat) + "kg");

            WriteRenderedTextToScreens(DisplayMode.reactor, text);
        }
        #endregion
        ///////////////////////////////
        #region energy
        private List<IMyWindTurbine> windTurbines = new List<IMyWindTurbine>();
        private List<IMyPowerProducer> powerProducers = new List<IMyPowerProducer>();

        private double allPowerProducersMaxOutput = 0;
        private double allPowerProducersCurrentOutput = 0;
        private double allPowerProducersCurrentOutputPercentage = 0;

        private double windTurbinesMaxOutput = 0;
        private double windTurbinesCurrentOutput = 0;
        private double windTurbinesCurrentOutputPercentage = 0;

        // Hydrogen engines and other (modded or new) blocks
        private double otherPowerProducersMaxOutput = 0;
        private double otherPowerProducersCurrentOutput = 0;
        private double otherPowerProducersCurrentOutputPercentage = 0;

        private void InitEnergy()
        {
            windTurbines.Clear();
            GridTerminalSystem.GetBlocksOfType<IMyWindTurbine>(windTurbines, CurrentGridOnly);
            powerProducers.Clear();
            GridTerminalSystem.GetBlocksOfType<IMyPowerProducer>(powerProducers, CurrentGridOnly);
        }

        private void UpdateEnergy()
        {
            UpdateEnergyProducer(windTurbines, out windTurbinesMaxOutput, out windTurbinesCurrentOutput, out windTurbinesCurrentOutputPercentage);
            UpdateEnergyProducer(powerProducers, out allPowerProducersMaxOutput, out allPowerProducersCurrentOutput, out allPowerProducersCurrentOutputPercentage);

            otherPowerProducersMaxOutput = allPowerProducersMaxOutput - (batteriesMaxOutput + solarMaxOutput + windTurbinesMaxOutput + reactorMaxOutput);
            otherPowerProducersCurrentOutput = allPowerProducersCurrentOutput - (batteriesOutput + solarCurrentOutput + windTurbinesCurrentOutput + reactorCurrentOutput);
            otherPowerProducersCurrentOutputPercentage = otherPowerProducersCurrentOutput / otherPowerProducersMaxOutput;
        }

        private void UpdateEnergyProducer(IReadOnlyList<IMyPowerProducer> list, out double maxOutput, out double currentOutput, out double currentOutputPercentage)
        {
            maxOutput = 0;
            currentOutput = 0;
            currentOutputPercentage = 0;

            if( list.Count == 0 ){
                return;
            }

            foreach (var item in list)
            {
                maxOutput += item.MaxOutput;
                currentOutput += item.CurrentOutput;
            }

            currentOutputPercentage = maxOutput == 0 ? 0 : currentOutput / maxOutput;
        }

        private void DisplayEnergy()
        {
            if (!HasDisplaysForMode(DisplayMode.energy)) return;

            var text = GetStringBuilderForScreen();
            CreateHeader(text, "Energy Output");

            //
            StringBuilder generation = new StringBuilder(DisplayWidth);
            generation.Append(allPowerProducersCurrentOutput.ToString(ThousandSeparatorNumberFormat));
            generation.Append('/');
            generation.Append(allPowerProducersMaxOutput.ToString(ThousandSeparatorNumberFormat));
            generation.Append(" MWh");
            CreateTwoColumnText(text, "Generation", generation.ToString());
            CreateBar(text, allPowerProducersCurrentOutputPercentage, false);
            text.Append('\n');

            if( batteries.Count > 0 ){
            CreateTwoColumnText(text, "Batteries", batteriesOutputPercentage.ToString(PercentageNumberFormat));
            }

            if( reactors.Count > 0 ){
            CreateTwoColumnText(text, "Reactors", reactorCurrentOutputPercentage.ToString(PercentageNumberFormat));
            }

            if( solarPanels.Count > 0 ){
            CreateTwoColumnText(text, "Solar", solarCurrentOutputPercentage.ToString(PercentageNumberFormat));
            }

            if( windTurbines.Count > 0 ){
            CreateTwoColumnText(text, "Wind", windTurbinesCurrentOutputPercentage.ToString(PercentageNumberFormat));
            }

            if( otherPowerProducersMaxOutput > 0 ){
                CreateTwoColumnText(text, "Hydrogen", otherPowerProducersCurrentOutputPercentage.ToString(PercentageNumberFormat));
            }

            text.Append('\n');

            var amountUranium = GetAmountOfResource(InventoryResourceType.Ingot, InventoryResourceName.Uranium);
            if (reactors.Count > 0 && amountUranium < 15) // TODO make this a setting
            {
                CreateCenteredText(text, "Uranium low!");
            }

            WriteRenderedTextToScreens(DisplayMode.energy, text);
        }
        #endregion
        ///////////////////////////////
        #region shipReference
        private IMyShipController mainShipController = null;
        private const string requiresMainShipController = "requires main cockpit";

        private void InitShipReference()
        {
            mainShipController = null;

            var shipControllers = new List<IMyShipController>();
            GridTerminalSystem.GetBlocksOfType<IMyShipController>(shipControllers, CurrentGridOnly);

            foreach (var shipController in shipControllers)
            {
                if (shipController.IsMainCockpit)
                {
                    mainShipController = shipController;
                    return;
                }
            }
        }
        #endregion
        ///////////////////////////////
        #region thrust
        #region thrust_helpers
        private class ThrustInformation
        {
            public float currentThrust = 0; // in N
            public float maxThrust = 0; // in N
            public float maxEffectiveThrust = 0; // in N

            public void Add(IMyThrust thruster)
            {
                currentThrust += thruster.CurrentThrust;
                maxThrust += thruster.MaxThrust;
                maxEffectiveThrust += thruster.MaxEffectiveThrust;
            }

            public void Clear()
            {
                currentThrust = 0;
                maxThrust = 0;
                maxEffectiveThrust = 0;
            }

        }

        private string getStringForThrusterDirection(Base6Directions.Direction direction)
        {
            switch (direction)
            {
                case Base6Directions.Direction.Up: return "Up";
                case Base6Directions.Direction.Down: return "Down";
                case Base6Directions.Direction.Left: return "Left";
                case Base6Directions.Direction.Right: return "Right";
                case Base6Directions.Direction.Forward: return "Forwd";
                case Base6Directions.Direction.Backward: return "Backwd";
                default: return "???";
            }
        }
        #endregion

        private Dictionary<Base6Directions.Direction, List<IMyThrust>> thrusterDirections = new Dictionary<Base6Directions.Direction, List<IMyThrust>>();
        private Dictionary<Base6Directions.Direction, ThrustInformation> thrusterInformation = new Dictionary<Base6Directions.Direction, ThrustInformation>();

        private bool ThrusterEnabled = false;

        private void InitThrust()
        {
            if (mainShipController == null)
                return;

            thrusterInformation.Clear();
            thrusterDirections.Clear();

            List<IMyThrust> thrusters = new List<IMyThrust>();
            GridTerminalSystem.GetBlocksOfType<IMyThrust>(thrusters, CurrentGridOnly);

            var directions = Enum.GetValues(typeof(Base6Directions.Direction));
            foreach (Base6Directions.Direction direction in directions)
            {
                thrusterDirections.Add(direction, new List<IMyThrust>());
                thrusterInformation.Add(direction, new ThrustInformation());
            }

            foreach (var thruster in thrusters)
            {
                // stolen from https://gist.github.com/ZerothAngel/da177f8a02347ac252b9
                var facing = thruster.Orientation.TransformDirection(Base6Directions.Direction.Forward); // Exhaust goes this way
                var thrustDirection = Base6Directions.GetFlippedDirection(facing);
                var shipDirection = mainShipController.Orientation.TransformDirectionInverse(thrustDirection);

                thrusterDirections[shipDirection].Add(thruster);
            }
        }

        private void UpdateThrust()
        {
            if (mainShipController == null)
                return;

            ThrusterEnabled = true;

            foreach (var direction in thrusterInformation.Keys)
            {
                thrusterInformation[direction].Clear();

                foreach (var thruster in thrusterDirections[direction])
                {
                    thrusterInformation[direction].Add(thruster);
                    if (!thruster.Enabled) ThrusterEnabled = false;
                }
            }
        }

        private void DisplayThrust()
        {
            if (!HasDisplaysForMode(DisplayMode.thrust)) return;

            var text = GetStringBuilderForScreen();
            CreateTwoColumnHeader(text, "Thrust", "eff/max");

            if (mainShipController == null)
            {
                text.Append('\n');
                CreateCenteredText(text, requiresMainShipController);
            }
            else
            {
                PrintThrustForPair(text, Base6Directions.Direction.Up, Base6Directions.Direction.Down);
                PrintThrustForPair(text, Base6Directions.Direction.Left, Base6Directions.Direction.Right);
                PrintThrustForPair(text, Base6Directions.Direction.Forward, Base6Directions.Direction.Backward);
            }

            WriteRenderedTextToScreens(DisplayMode.thrust, text);
        }

        private void PrintThrustForPair(StringBuilder text, Base6Directions.Direction left, Base6Directions.Direction right)
        {
            var leftThrustPercentage = GetThrustPercentageFor(left);
            var rightThrustPercentage = GetThrustPercentageFor(right);

            PrintThrustInfoLine(text, left);
            CreateBar(text, leftThrustPercentage, rightThrustPercentage, false, true);
            PrintThrustInfoLine(text, right);

            //CreateDoubleBar(text, leftThrustPercentage, rightThrustPercentage);
            // text.Append('\n');
        }

        private void PrintThrustInfoLine(StringBuilder text, Base6Directions.Direction direction)
        {
            var info = thrusterInformation[direction];
            var title = getStringForThrusterDirection(direction);

            const string noThrust = "0";
            string currToMaxEffectivePercentage = info.maxEffectiveThrust == 0 ? noThrust : (info.currentThrust / info.maxEffectiveThrust).ToString(PercentageNumberFormat);
            string currToMaxPercentage = info.maxThrust == 0 ? noThrust : (info.currentThrust / info.maxThrust).ToString(PercentageNumberFormat);

            CreateTwoColumnText(text, title, currToMaxEffectivePercentage + "/" + currToMaxPercentage);
        }

        private double GetThrustPercentageFor(Base6Directions.Direction direction)
        {
            var info = thrusterInformation[direction];
            return info.currentThrust / info.maxThrust;
        }

        #endregion
        ///////////////////////////////
        #region thrust_weight
        private double thrustWeightDownwardForceInN = 0;
        private double thrustNecessaryDownFromPitchPercentage;
        private double thrustNecessaryDownFromRollPercentage;
        private double thrustNecessaryDownTotalInN;
        private double thrustFrontNecessaryInN;
        private double thrustBackNecessaryInN;
        private double thrustRightNecessaryInN;
        private double thrustLeftNecessaryInN;

        private void UpdateThrustWeight()
        {
            if (mainShipController == null){
                return;
            }

            this.CalculateOrientation();
            
            thrustWeightDownwardForceInN = envIsInPlanetReach ? envShipTotalMassInKg * envCurrentGravity : 0;

            thrustNecessaryDownFromPitchPercentage = 1 - (Math.Min(Math.Max(Math.Abs(pitch),0), 90) / 90); //Math.Cos(ToRadians(Math.Abs(pitch)));
            thrustNecessaryDownFromRollPercentage = 1 - (Math.Min(Math.Max(Math.Abs(roll),0), 90) / 90); // Math.Cos(ToRadians(Math.Abs(roll)));
            thrustNecessaryDownTotalInN =  thrustNecessaryDownFromPitchPercentage * thrustNecessaryDownFromRollPercentage * thrustWeightDownwardForceInN;

            //var thrustFrontNecessary =  (Math.Sin(ToRadians(Math.Abs(pitch))) * thrustWeightDownwardForceInN); // SE seems to calculate thrust linearly
            thrustFrontNecessaryInN =  pitch < 0 ? ((Math.Min(Math.Max(Math.Abs(pitch),0), 90) / 90)) * thrustWeightDownwardForceInN : 0;

            //var thrustBackNecessary =  (Math.Sin(ToRadians(Math.Abs(pitch))) * thrustWeightDownwardForceInN);
            thrustBackNecessaryInN =  pitch > 0 ? ((Math.Min(Math.Max(Math.Abs(pitch),0), 90) / 90)) * thrustWeightDownwardForceInN : 0;

            //var thrustRightNecessary =  (Math.Sin(ToRadians(Math.Abs(roll))) * thrustWeightDownwardForceInN);
            thrustRightNecessaryInN =  roll < 0 ? ((Math.Min(Math.Max(Math.Abs(roll),0), 90) / 90)) * thrustWeightDownwardForceInN : 0;
            
            //var thrustLeftNecessary =  (Math.Sin(ToRadians(Math.Abs(roll))) * thrustWeightDownwardForceInN);
            thrustLeftNecessaryInN =  roll > 0 ?((Math.Min(Math.Max(Math.Abs(roll),0), 90) / 90)) * thrustWeightDownwardForceInN : 0;
        }

        private void DisplayThrustWeight()
        {
            if (!HasDisplaysForMode(DisplayMode.thrustweight)) return;

            var text = GetStringBuilderForScreen();

            CreateTwoColumnHeader(text, envIsInPlanetReach ? "Thrust/Weight" : "Thrust", envIsInPlanetReach ? "(eff/max)" : "(kN)");

            if (mainShipController == null)
            {
                text.Append('\n');
                CreateCenteredText(text, requiresMainShipController);
            }
            else
            {
                foreach (var entry in thrusterInformation)
                {
                    var title = getStringForThrusterDirection(entry.Key);
                    
                    if (envIsInPlanetReach) 
                    {
                        string downforceToMaxThrustRatio = (entry.Value.maxThrust / thrustWeightDownwardForceInN).ToString(DoubleDecimalPlacesFormat);
                        string downforceToMaxEffectiveThrustRatio = (entry.Value.maxEffectiveThrust / thrustWeightDownwardForceInN).ToString(DoubleDecimalPlacesFormat);

                        CreateTwoColumnText(text, title, downforceToMaxEffectiveThrustRatio + " / " + downforceToMaxThrustRatio);
                    }
                    else
                    {
                        string maxThrustkN = (entry.Value.maxThrust / 1000).ToString(ThousandSeparatorNumberFormat);
                        CreateTwoColumnText(text, title, maxThrustkN + " kN");
                    }
                }

                //text.Append('\n');
                //CreateTwoColumnText(text, "Mass", (envShipTotalMassInKg / 1000).ToString(ThousandSeparatorNumberFormat) + " t");

                // Pitch/Roll guard is only available in planetary reach
                if (envIsInPlanetReach){ 
                    this.CalculateOrientation();

                    //text.Append('\n');
                    //CreateTwoColumnText(text, "Pitch", pitch.ToString(DoubleDecimalPlacesFormat));
                    //CreateTwoColumnText(text, "Roll", roll.ToString(DoubleDecimalPlacesFormat));

                    text.Append('\n');
                    CreateTwoColumnText(text, "Down", (thrustNecessaryDownTotalInN / 1000).ToString(DoubleDecimalPlacesFormat) + " kN");
                    CheckAndPrintThrustWarning(text, thrustNecessaryDownTotalInN, Base6Directions.Direction.Up);

                    if( pitch < 0 ){
                        CreateTwoColumnText(text, "Front", (thrustFrontNecessaryInN / 1000).ToString(DoubleDecimalPlacesFormat) + " kN");
                        CheckAndPrintThrustWarning(text, thrustFrontNecessaryInN, Base6Directions.Direction.Forward);
                    }else{
                        CreateTwoColumnText(text, "Back", (thrustBackNecessaryInN / 1000).ToString(DoubleDecimalPlacesFormat) + " kN");
                        CheckAndPrintThrustWarning(text, thrustBackNecessaryInN, Base6Directions.Direction.Backward);
                    }

                    if( roll < 0 ){
                        CreateTwoColumnText(text, "Right", (thrustRightNecessaryInN / 1000).ToString(DoubleDecimalPlacesFormat) + " kN");
                        CheckAndPrintThrustWarning(text, thrustRightNecessaryInN, Base6Directions.Direction.Right);
                    }else{
                        CreateTwoColumnText(text, "Left", (thrustLeftNecessaryInN / 1000).ToString(DoubleDecimalPlacesFormat) + " kN");
                        CheckAndPrintThrustWarning(text, thrustLeftNecessaryInN, Base6Directions.Direction.Left);
                    }
                }
                
                

            }

            WriteRenderedTextToScreens(DisplayMode.thrustweight, text);
        }

        private void CheckAndPrintThrustWarning( StringBuilder text, double neccessaryThrust, Base6Directions.Direction direction ){
            var percentage = neccessaryThrust / thrusterInformation[direction].maxEffectiveThrust;
            if( percentage >= 1 ){
                CreateCenteredText(text, "!!" + getStringForThrusterDirection(direction) + " Thrust Limit Reached!!");
            }else if( percentage >= 0.9 ){
                CreateCenteredText(text, "!" + getStringForThrusterDirection(direction) + " Thrust Limit Close!");
            }
        }

        // Copied from https://github.com/Naosyth/SpaceEngineersPrograms/blob/71db925622be82a55ef23946bbacbedaae211cbb/helicopter.cs#L187C25-L187C25 (THANK YOU!)
        private double pitch = 0;
        private double roll = 0;
        private void CalculateOrientation() {
            if ( !envIsInPlanetReach ) {
                pitch = 0;
                roll = 0;
                return ;
            }

            Vector3 gravityVec = -Vector3.Normalize(mainShipController.GetNaturalGravity());

            var orientation = mainShipController.WorldMatrix;
            var forwardVec = orientation.Forward;
            var rightVec = orientation.Right;
            var upVec = orientation.Up;

            pitch = Vector3.Dot(gravityVec, forwardVec) / (gravityVec.Length() * forwardVec.Length()) * 90;
            roll = Vector3.Dot(gravityVec, rightVec) / (gravityVec.Length() * rightVec.Length()) * 90;
        }

    private double ToRadians(double val)
    {
        return (Math.PI / 180) * val;
    }

        #endregion
        ///////////////////////////////
        #region environment
        private double envCurrentGravity = 0;
        private double environmentCurrentSpeed = 0;

        private bool envIsInPlanetReach = false;
        private double envSeaLevelElevation = 0;
        private double envSurfaceLevelElevation = 0;

        private float envShipTotalMassInKg = 0f;

        private IMyAirVent envOutsideVent = null;
        private float envOutsideAirPressure = 0f;

        private void InitEnvironment()
        {
            envCurrentGravity = 0;
            environmentCurrentSpeed = 0;

            envOutsideVent = null;
            List<IMyAirVent> airVents = new List<IMyAirVent>();
            GridTerminalSystem.GetBlocksOfType<IMyAirVent>(airVents, CurrentGridOnly);
            foreach (var vent in airVents)
            {
                var properties = GetKeyValuePairsFromString(vent.CustomData);
                if (properties.ContainsKey("vent") && properties["vent"] == "environment")
                {
                    envOutsideVent = vent;
                    break;
                }
            }
        }

        private void UpdateEnvironment10()
        {
            envOutsideAirPressure = envOutsideVent != null ? envOutsideVent.GetOxygenLevel() : 0f;

            if (mainShipController == null) return;
            environmentCurrentSpeed = mainShipController.GetShipSpeed();
        }

        private void UpdateEnvironment100()
        {
            // auto fill oxygen tanks when in atmosphere
            if (envOutsideVent != null && envOutsideAirPressure > 0)
                envOutsideVent.Depressurize = gasStorageTotalFillPercentage[GasType.Oxygen] < 1;

            if (mainShipController == null) return;

            envCurrentGravity = mainShipController.GetNaturalGravity().Length();

            envIsInPlanetReach = (
                mainShipController.TryGetPlanetElevation(MyPlanetElevation.Sealevel, out envSeaLevelElevation)
                && mainShipController.TryGetPlanetElevation(MyPlanetElevation.Surface, out envSurfaceLevelElevation)
            );

            var massInformation = mainShipController.CalculateShipMass();
            envShipTotalMassInKg = massInformation.TotalMass;
        }

        private void DisplayEnvironment()
        {
            if (!HasDisplaysForMode(DisplayMode.environment)) return;

            var text = GetStringBuilderForScreen();
            CreateHeader(text, "Environment");

            if (mainShipController == null)
            {
                text.Append('\n');
                CreateCenteredText(text, requiresMainShipController);
                text.Append("\n");
            }
            else
            {

                CreateTwoColumnText(text, "Gravity (m/s" + SUP_TWO + ")", envCurrentGravity.ToString(DoubleDecimalPlacesFormat));

                CreateTwoColumnText(text, "Speed (m/s)", environmentCurrentSpeed.ToString(DoubleDecimalPlacesFormat));
                text.Append('\n');
                CreateBar(text, environmentCurrentSpeed / ShipMaxSpeed, false);
                text.Append("\n");

                CreateTwoColumnText(text, "Mass", envShipTotalMassInKg.ToString(ThousandSeparatorNumberFormat));
                text.Append("\n");

                if (envIsInPlanetReach)
                {
                    CreateTwoColumnText(text, "Surface", envSurfaceLevelElevation.ToString(ThousandSeparatorNumberFormat));
                    CreateTwoColumnText(text, "Sea Level", envSeaLevelElevation.ToString(ThousandSeparatorNumberFormat));
                    text.Append("\n");
                }
            }

            if (envOutsideVent != null)
            {
                CreateTwoColumnText(text, "Air pressure", envOutsideAirPressure.ToString(PercentageNumberFormat));
            }

            WriteRenderedTextToScreens(DisplayMode.environment, text);
        }
        #endregion
        ///////////////////////////////
        #region inventories
        private List<IMyInventory> inventories = new List<IMyInventory>();
        private double inventoriesMaxVolume = 0;

        private double inventoriesCurrentVolume = 0;
        private double inventoriesCurrentMass = 0;
        private double inventoriesFillPercentage = 0;

        private enum InventoryResourceType { Ore, Ingot };
        private enum InventoryResourceName { Iron, Nickel, Cobalt, Gold, Silver, Silicon, Uranium, Platinum, Stone, Magnesium, Ice };

        private Dictionary<InventoryResourceType, Dictionary<InventoryResourceName, double>> inventoryResources = new Dictionary<InventoryResourceType, Dictionary<InventoryResourceName, double>>();

        List<MyInventoryItem> inventoryTmpItemsList = new List<MyInventoryItem>(); // tmp list to avoid reallocation every update

        private void InitInventories()
        {
            inventories.Clear();
            inventoryResources.Clear();

            inventoryResources.Add(InventoryResourceType.Ore, new Dictionary<InventoryResourceName, double>());
            inventoryResources.Add(InventoryResourceType.Ingot, new Dictionary<InventoryResourceName, double>());

            this.inventoriesMaxVolume = 0;
            long inventoriesMaxVolume = 0;

            List<IMyTerminalBlock> potentialInventoryHolders = new List<IMyTerminalBlock>();
            GridTerminalSystem.GetBlocksOfType<IMyTerminalBlock>(potentialInventoryHolders, CurrentGridOnly);

            foreach (var potentialInventoryHolder in potentialInventoryHolders)
            {
                if (!potentialInventoryHolder.HasInventory)
                    continue;

                for (int i = 0; i < potentialInventoryHolder.InventoryCount; i++)
                {
                    IMyInventory inventory = potentialInventoryHolder.GetInventory(i);
                    inventories.Add(inventory);
                    inventoriesMaxVolume += inventory.MaxVolume.RawValue;
                }
            }

            this.inventoriesMaxVolume = inventoriesMaxVolume / 1000000d;
        }

        private void UpdateInventories()
        {
            inventoryResources[InventoryResourceType.Ore].Clear();
            inventoryResources[InventoryResourceType.Ingot].Clear();

            long inventoriesCurrentVolume = 0;
            long inventoriesCurrentMass = 0;

            foreach (var inventory in inventories)
            {
                inventoriesCurrentVolume += inventory.CurrentVolume.RawValue;
                inventoriesCurrentMass += inventory.CurrentMass.RawValue;

                inventoryTmpItemsList.Clear();
                inventory.GetItems(inventoryTmpItemsList);
                InventoryAddItems(inventoryTmpItemsList);
            }

            this.inventoriesCurrentVolume = inventoriesCurrentVolume / 1000000d;
            this.inventoriesCurrentMass = inventoriesCurrentMass / 1000000d;
            inventoriesFillPercentage = this.inventoriesMaxVolume > 0 ? this.inventoriesCurrentVolume / this.inventoriesMaxVolume : 0;
        }

        private void InventoryAddItems(List<MyInventoryItem> items)
        {
            foreach (var item in items)
            {
                InventoryResourceName resource;
                if (!InventoryResourceName.TryParse(item.Type.SubtypeId, out resource))
                    continue;

                InventoryResourceType type;
                if (item.Type.TypeId.EndsWith("Ore"))
                {
                    type = InventoryResourceType.Ore;
                }
                else if (item.Type.TypeId.EndsWith("Ingot"))
                {
                    type = InventoryResourceType.Ingot;
                }
                else
                {
                    continue;
                }

                var list = inventoryResources[type];

                if (!list.ContainsKey(resource))
                    list.Add(resource, 0);

                list[resource] += item.Amount.RawValue / 1000000d;
            }
        }

        private void DisplayInventory()
        {
            DisplayInventoryOverview();

            DisplayInventoryResource(DisplayMode.ingots, InventoryResourceType.Ingot, 2);
            DisplayInventoryResource(DisplayMode.ores, InventoryResourceType.Ore, 3);
        }

        private void DisplayInventoryOverview()
        {
            if (!HasDisplaysForMode(DisplayMode.inventory)) return;

            var text = GetStringBuilderForScreen();
            CreateHeader(text, "Inventory");

            CreateTwoColumnText(text, "Total", inventoriesFillPercentage.ToString(PercentageNumberFormat));

            StringBuilder volume = new StringBuilder(DisplayWidth);
            volume.Append(inventoriesCurrentVolume.ToString(ThousandSeparatorNumberFormatWithSingleDecimalPlace));
            volume.Append("/");
            volume.Append(inventoriesMaxVolume.ToString(ThousandSeparatorNumberFormatWithSingleDecimalPlace));
            volume.Append(QM);
            CreateTwoColumnText(text, string.Empty, volume.ToString());
            text.Append('\n');

            CreateBar(text, inventoriesFillPercentage, false);

            // Show list of ores in miner mode
            if (SelectedProgramMode == ProgramMode.Miner && inventoryResources.ContainsKey(InventoryResourceType.Ore))
            {
                text.Append('\n');
                foreach (var resource in inventoryResources[InventoryResourceType.Ore])
                {
                    CreateTwoColumnText(text, resource.Key.ToString(), resource.Value.ToString(ThousandSeparatorNumberFormatWithSingleDecimalPlace));
                }
            }

            WriteRenderedTextToScreens(DisplayMode.inventory, text);
        }

        private void DisplayInventoryResource(DisplayMode mode, InventoryResourceType resourceType, int page)
        {
            if (!HasDisplaysForMode(mode)) return;

            var text = GetStringBuilderForScreen();
            CreateTwoColumnHeader(text, resourceType.ToString(), "kg");

            foreach (var resource in inventoryResources[resourceType])
            {
                CreateTwoColumnText(text, resource.Key.ToString(), resource.Value.ToString(ThousandSeparatorNumberFormatWithSingleDecimalPlace));
            }

            WriteRenderedTextToScreens(mode, text);
        }

        private bool HasResource(InventoryResourceType type, InventoryResourceName resource)
        {
            return inventoryResources[type].ContainsKey(resource);
        }

        private double GetAmountOfResource(InventoryResourceType type, InventoryResourceName resource)
        {
            if (!HasResource(type, resource))
                return 0;
            return inventoryResources[type][resource];
        }

        #endregion
        ///////////////////////////////
        #region rooms
        private Dictionary<string, List<IMyAirVent>> roomVents = new Dictionary<string, List<IMyAirVent>>();
        private Dictionary<string, float> roomPressureLevels = new Dictionary<string, float>();

        private void InitRooms()
        {
            roomVents.Clear();

            List<IMyAirVent> vents = new List<IMyAirVent>();
            GridTerminalSystem.GetBlocksOfType<IMyAirVent>(vents, CurrentGridOnly);

            foreach (var vent in vents)
            {
                var properties = GetKeyValuePairsFromString(vent.CustomData);

                if (!properties.ContainsKey("room"))
                    continue;

                string roomName = properties["room"];

                if (!roomVents.ContainsKey(roomName))
                {
                    roomVents[roomName] = new List<IMyAirVent>();
                }

                roomVents[roomName].Add(vent);
            }

        }

        private void UpdateRooms()
        {

        }

        private float getPressureLevelForRoom(string roomName)
        {
            if (!roomVents.ContainsKey(roomName))
                return 0f;

            return roomVents[roomName][0].GetOxygenLevel();
        }

        private void DisplayRooms()
        {
            if (!HasDisplaysForMode(DisplayMode.rooms)) return;

            var text = GetStringBuilderForScreen();
            CreateTwoColumnHeader(text, "Rooms", "Pressure");

            foreach (var roomName in roomVents.Keys)
            {
                string status = getPressureLevelForRoom(roomName).ToString(PercentageNumberFormat);
                CreateTwoColumnText(text, roomName, status);
            }

            WriteRenderedTextToScreens(DisplayMode.rooms, text);
        }
        #endregion
        ///////////////////////////////
        #region lights
        private Dictionary<string, List<IMyLightingBlock>> lights = new Dictionary<string, List<IMyLightingBlock>>();

        private void InitLights()
        {
            lights.Clear();
            List<IMyLightingBlock> allLights = new List<IMyLightingBlock>();

            GridTerminalSystem.GetBlocksOfType<IMyLightingBlock>(allLights, CurrentGridOnly);

            foreach (var light in allLights)
            {
                var properties = GetKeyValuePairsFromString(light.CustomData);

                if (!properties.ContainsKey("room"))
                    continue;

                string roomName = properties["room"];

                if (!lights.ContainsKey(roomName))
                {
                    lights[roomName] = new List<IMyLightingBlock>();
                }

                lights[roomName].Add(light);
            }

        }

        private void UpdateLights()
        {
            // Nothing to do
        }

        private void CommandLights(string argument)
        {
            string[] arguments = argument.Split(' ');

            if (arguments.Length != 2)
                return;

            string roomName = arguments[1];

            if (!lights.ContainsKey(roomName))
                return;

            var roomLights = lights[roomName];

            switch (arguments[0])
            {
                case "on":
                    roomLights.ForEach(l => l.Enabled = true);
                    break;

                case "off":
                    roomLights.ForEach(l => l.Enabled = false);
                    break;

                case "toggle":
                    roomLights.ForEach(l => l.Enabled = !l.Enabled);
                    break;
            }
        }
        #endregion
        ///////////////////////////////
        #region airlocks
        private Dictionary<string, List<IMyDoor>> airlockDoors = new Dictionary<string, List<IMyDoor>>();
        private Dictionary<IMyDoor, int> airlockOpenDoorDelays = new Dictionary<IMyDoor, int>();
        private Dictionary<IMyDoor, string> airlockDoorToRoom = new Dictionary<IMyDoor, string>();

        private void InitAirlocks()
        {
            airlockDoors.Clear();
            airlockOpenDoorDelays.Clear();
            airlockDoorToRoom.Clear();

            List<IMyDoor> doors = new List<IMyDoor>();
            GridTerminalSystem.GetBlocksOfType<IMyDoor>(doors, CurrentGridOnly);

            foreach (var door in doors)
            {
                var properties = GetKeyValuePairsFromString(door.CustomData);

                if (!properties.ContainsKey("airlock"))
                    continue;

                string groupName = properties["airlock"];

                door.CloseDoor();

                if (!this.airlockDoors.ContainsKey(groupName))
                {
                    this.airlockDoors.Add(groupName, new List<IMyDoor>());
                }

                this.airlockDoors[groupName].Add(door);

                if (properties.ContainsKey("room"))
                {
                    string roomName = properties["room"];
                    this.airlockDoorToRoom.Add(door, roomName);
                }
            }
        }

        private void UpdateAirlocks()
        {
            // update delay left
            var doors = new List<IMyDoor>(airlockOpenDoorDelays.Keys);
            foreach (var door in doors)
            {
                airlockOpenDoorDelays[door] = Math.Max(0, airlockOpenDoorDelays[door] - 10);
            }

            // update all door groups (aka airlocks)
            foreach (string group in airlockDoors.Keys)
            {
                bool allDoorsClosed = true;

                foreach (var door in airlockDoors[group])
                {
                    if (door.Status != DoorStatus.Closed)
                    {
                        allDoorsClosed = false;
                        break;
                    }
                }

                bool openDoorsInGroupAreSafe = true;
                foreach (IMyDoor door in airlockDoors[group])
                {
                    if (door.Status == DoorStatus.Closed)
                    {
                        continue;
                    }

                    // if an open airlock door does not have a room defined,
                    // it is treated as a door to outside.
                    if (!airlockDoorToRoom.ContainsKey(door))
                    {
                        if (envOutsideAirPressure < MIN_PRESSURE_LEVEL)
                        {
                            openDoorsInGroupAreSafe = false;
                            break;
                        }

                        continue;
                    }

                    float pressure = getPressureLevelForRoom(airlockDoorToRoom[door]);
                    if (pressure < MIN_PRESSURE_LEVEL)
                    {
                        openDoorsInGroupAreSafe = false;
                        break;
                    }
                }

                foreach (IMyDoor door in airlockDoors[group])
                {
                    // only send command to open doors
                    if (door.Status == DoorStatus.Open)
                    {
                        // if the door is not within the ticks left open dict,
                        // the door is open for the first iteration.
                        // put it in with the value of the set delay
                        if (!airlockOpenDoorDelays.ContainsKey(door))
                        {
                            airlockOpenDoorDelays.Add(door, AirlockAutoClosingDelay);
                            continue;
                        }

                        // if the ticks left open are larger than 0,
                        // wait until the ticks are reached
                        if (airlockOpenDoorDelays[door] > 0)
                        {
                            continue;
                        }

                        // when ticks are reached, close the door shut
                        airlockOpenDoorDelays.Remove(door);
                        door.CloseDoor();
                    }

                    // Only disable closed doors
                    if (door.Status == DoorStatus.Closed)
                    {
                        // if all open doors are save, the airlock is set up to be connected to a room and the pressure level in that room is safe,
                        // it does not need to be disabled.
                        bool otherSideIsSafe = airlockDoorToRoom.ContainsKey(door) ? getPressureLevelForRoom(airlockDoorToRoom[door]) >= MIN_PRESSURE_LEVEL : envOutsideAirPressure >= MIN_PRESSURE_LEVEL;
                        if (openDoorsInGroupAreSafe && otherSideIsSafe)
                        {
                            door.Enabled = true;
                        }
                        else
                        {
                            door.Enabled = allDoorsClosed;
                        }
                    }

                }
            }
        }

        private void DisplayAirlocks()
        {
            if (!HasDisplaysForMode(DisplayMode.airlocks)) return;

            var text = GetStringBuilderForScreen();
            CreateHeader(text, "Airlocks");

            foreach (var groupName in airlockDoors.Keys)
            {
                text.Append("- ");
                text.Append(groupName);
                text.Append('\n');
            }

            WriteRenderedTextToScreens(DisplayMode.airlocks, text);
        }
        #endregion
        ///////////////////////////////
        #region hangars
        private Dictionary<string, List<IMyAirtightHangarDoor>> hangarDoors = new Dictionary<string, List<IMyAirtightHangarDoor>>();
        private Dictionary<string, List<IMyTextPanel>> hangarWarningDisplays = new Dictionary<string, List<IMyTextPanel>>();
        private Dictionary<string, List<IMyLightingBlock>> hangarWarningLights = new Dictionary<string, List<IMyLightingBlock>>();
        private Dictionary<string, DoorStatus> hangarStatus = new Dictionary<string, DoorStatus>();

        private void InitHangars()
        {
            hangarDoors.Clear();
            hangarStatus.Clear();
            hangarWarningDisplays.Clear();
            hangarWarningLights.Clear();

            List<IMyAirtightHangarDoor> doors = new List<IMyAirtightHangarDoor>();
            GridTerminalSystem.GetBlocksOfType<IMyAirtightHangarDoor>(doors, CurrentGridOnly);
            foreach (var door in doors)
            {
                var properties = GetKeyValuePairsFromString(door.CustomData);

                if (!properties.ContainsKey("hangar"))
                    continue;

                var roomName = properties["hangar"];

                bool isOpen = GetSetting("hangar_" + roomName, false);

                if (!hangarDoors.ContainsKey(roomName))
                {
                    hangarDoors.Add(roomName, new List<IMyAirtightHangarDoor>());
                    hangarStatus.Add(roomName, isOpen ? DoorStatus.Opening : DoorStatus.Closing);
                }

                hangarDoors[roomName].Add(door);
                if (isOpen)
                {
                    door.OpenDoor();
                }
                else
                {
                    door.CloseDoor();
                }
            }

            List<IMyTextPanel> hangarDisplays = new List<IMyTextPanel>();
            GridTerminalSystem.GetBlocksOfType<IMyTextPanel>(hangarDisplays, CurrentGridOnly);
            foreach (var display in hangarDisplays)
            {
                var properties = GetKeyValuePairsFromString(display.CustomData);

                if (!properties.ContainsKey("hangar"))
                    continue;

                var roomName = properties["hangar"];

                if (!hangarWarningDisplays.ContainsKey(roomName))
                {
                    hangarWarningDisplays.Add(roomName, new List<IMyTextPanel>());
                }

                hangarWarningDisplays[roomName].Add(display);
            }

            List<IMyLightingBlock> lights = new List<IMyLightingBlock>();
            GridTerminalSystem.GetBlocksOfType<IMyLightingBlock>(lights, CurrentGridOnly);
            foreach (var light in lights)
            {
                var properties = GetKeyValuePairsFromString(light.CustomData);

                if (!properties.ContainsKey("hangar"))
                    continue;

                var roomName = properties["hangar"];

                if (!hangarWarningLights.ContainsKey(roomName))
                {
                    hangarWarningLights.Add(roomName, new List<IMyLightingBlock>());
                }

                hangarWarningLights[roomName].Add(light);
                light.Enabled = false;
            }

        }

        private void SetHangarWarnLights(string roomName, bool enabled)
        {
            if (hangarWarningLights.ContainsKey(roomName))
            {
                foreach (var light in hangarWarningLights[roomName])
                {
                    light.Enabled = enabled;
                }
            }
        }

        private void SetHangarDisplays(string roomName, DoorStatus status)
        {
            if (hangarWarningDisplays.ContainsKey(roomName))
            {
                Color color = Color.White;
                string text = string.Empty;

                switch (status)
                {
                    case DoorStatus.Open:
                        color = COLOR_RED;
                        text = "Hangar open";
                        break;

                    case DoorStatus.Opening:
                        color = COLOR_ORANGE;
                        text = "Hangar opening";
                        break;

                    case DoorStatus.Closing:
                        color = COLOR_ORANGE;
                        text = "Hangar closing";
                        break;

                    case DoorStatus.Closed:
                        color = COLOR_GREEN;
                        text = "Hangar closed";
                        break;
                }

                foreach (var display in hangarWarningDisplays[roomName])
                {
                    display.FontColor = color;
                    display.FontSize = HANGAR_FONT_SIZE;
                    display.WriteText(text);
                }
            }
        }

        private void ChangeHangarState(string roomName, DoorStatus oldState, DoorStatus newState)
        {
            hangarStatus[roomName] = newState;
            SetHangarDisplays(roomName, newState);

            if (newState == DoorStatus.Closed)
            {
                SetHangarWarnLights(roomName, false);
            }
            else if (newState == DoorStatus.Open)
            {
                SetHangarWarnLights(roomName, false);
            }
            else if (newState == DoorStatus.Closing)
            {
                SetHangarWarnLights(roomName, true);
            }
            else if (newState == DoorStatus.Opening)
            {
                SetHangarWarnLights(roomName, true);
            }
        }

        private void UpdateHangars()
        {
            foreach (var roomName in hangarDoors.Keys)
            {
                switch (hangarStatus[roomName])
                {
                    case DoorStatus.Closing:
                        // First, check if all doors are closed
                        bool allDoorsClosed = true;
                        foreach (var door in hangarDoors[roomName])
                        {
                            if (door.Status != DoorStatus.Closed)
                            {
                                door.CloseDoor();
                                allDoorsClosed = false;
                            }
                        }

                        if (!allDoorsClosed)
                            break;

                        // Next check if the room is pressurized
                        // (only if the room has vents)
                        bool roomIsPressurized = true;
                        if (roomVents.ContainsKey(roomName))
                        {
                            foreach (var vent in roomVents[roomName])
                            {
                                // start pressurization
                                if (vent.Depressurize)
                                {
                                    vent.Depressurize = false;
                                }

                                if (vent.GetOxygenLevel() != 1f)
                                {
                                    roomIsPressurized = false;
                                }

                            }
                        }

                        if (!roomIsPressurized)
                            break;

                        // if doors are closed and room is pressurized, we change to close state
                        ChangeHangarState(roomName, hangarStatus[roomName], DoorStatus.Closed);

                        break;

                    case DoorStatus.Closed:
                        // idle
                        break;

                    case DoorStatus.Opening:

                        // Next check if the room is pressurized
                        // (only if the room has vents)
                        bool roomIsDepressurized = true;
                        if (roomVents.ContainsKey(roomName))
                        {
                            foreach (var vent in roomVents[roomName])
                            {
                                // start depressurization
                                if (!vent.Depressurize)
                                {
                                    vent.Depressurize = true;
                                }

                                if (vent.GetOxygenLevel() != 0)
                                {
                                    roomIsDepressurized = false;
                                }

                            }

                            // skip depressurization if oxygen tanks are 100% full
                            if (gasStorageAvailableFillPercentage[GasType.Oxygen] == 1)
                            {
                                roomIsDepressurized = true;
                            }
                        }

                        if (!roomIsDepressurized)
                            break;

                        bool allDoorsOpen = true;
                        foreach (var door in hangarDoors[roomName])
                        {
                            if (door.Status != DoorStatus.Open)
                            {
                                door.OpenDoor();
                                allDoorsOpen = false;
                            }
                        }

                        if (!allDoorsOpen)
                            break;

                        // Disable pressurize before changing to open state
                        // otherweise they would keep depressurizing while the hangar is open,
                        // which doesn't do anything except filling the oxygen tanks in atmospheres,
                        // but the constant depressurize fog animation can be annoying
                        // and the outside vent can fill the oxygen tanks within atmospheres too.
                        foreach (var vent in roomVents[roomName])
                            vent.Depressurize = false;

                        // if doors are open and room is depressurized, we change to open state
                        ChangeHangarState(roomName, hangarStatus[roomName], DoorStatus.Open);

                        break;

                    case DoorStatus.Open:
                        // nothing to do
                        break;
                }
            }
        }

        private bool AllHangarsClosed()
        {
            foreach (var roomName in hangarDoors.Keys)
                if (hangarStatus[roomName] != DoorStatus.Closed)
                    return false;
            return true;
        }

        private void CommandHangar(string argument)
        {
            var argumentList = argument.Split(' ');

            if (argumentList.Length < 2) return;

            switch (argumentList[0])
            {
                case "open":
                    OpenCloseHangar(argumentList[1], true);
                    break;

                case "close":
                    OpenCloseHangar(argumentList[1], false);
                    break;
            }

        }

        private void OpenCloseAllHangars(bool open = true)
        {
            foreach (var hangar in hangarDoors)
            {
                OpenCloseSingleHangar(hangar.Key, open);
            }
        }

        private void OpenCloseSingleHangar(string roomName, bool open = true)
        {
            if (!hangarDoors.ContainsKey(roomName)) return;
            ChangeHangarState(roomName, hangarStatus[roomName], open ? DoorStatus.Opening : DoorStatus.Closing);
            SetSetting("hangar_" + roomName, open);
        }

        private void OpenCloseHangar(string roomName, bool open = true)
        {
            if (roomName == "all")
            {
                Echo((open ? "Opening" : "Closing") + " all hangars");
                OpenCloseAllHangars(open);
            }
            else {
                Echo((open ? "Opening" : "Closing") + " single hangar");
                OpenCloseSingleHangar(roomName, open);
            }
        }

        private void DisplayHangars()
        {
            if (!HasDisplaysForMode(DisplayMode.hangars)) return;

            var text = GetStringBuilderForScreen();
            CreateHeader(text, "Hangars");

            foreach (var roomName in hangarDoors.Keys)
            {
                text.Append(roomName);
                text.Append('\n');
                string oxygenLevel = roomVents.ContainsKey(roomName) ? roomVents[roomName][0].GetOxygenLevel().ToString(PercentageNumberFormat) : "";
                CreateTwoColumnText(text, hangarStatus[roomName].ToString(), oxygenLevel);
            }

            WriteRenderedTextToScreens(DisplayMode.hangars, text);
        }

        #endregion
        ///////////////////////////////
        #region autopilot
        private bool autopilotEnabled = true;
        private bool AutoOffEnabled = true;

        private bool DampenersEnabled = false;
        private bool ShipIsControlled = false;
        private bool ShipIsShutDown = false;

        private const string apEnabledKey = "ap_enabled";
        private const string apAutoOffKey = "ap_auto_off";

        private void InitAutopilot()
        {
            autopilotEnabled = GetSetting(apEnabledKey, true);
            AutoOffEnabled = GetSetting(apAutoOffKey, true);
        }

        private void UpdateAutopilot()
        {
            if (mainShipController == null) return;

            DampenersEnabled = mainShipController.DampenersOverride;
            ShipIsControlled = mainShipController.IsUnderControl;

            if (!autopilotEnabled) return;

            if (envIsInPlanetReach && !ShipIsControlled)
            {
                mainShipController.DampenersOverride = true;
            }

            // auto on/off ship
            if (AutoOffEnabled && SelectedProgramMode == ProgramMode.Miner)
            {
                if( ShipIsShutDown && (ShipIsControlled || !ShipIsDocked))
                {
                    AutpilotAutodockSetBatteriesAndThrusters(true);
                    ShipIsShutDown = false;
                }
                else if( !ShipIsShutDown && !ShipIsControlled && ShipIsDocked)
                {
                    AutpilotAutodockSetBatteriesAndThrusters(false);
                    ShipIsShutDown = true;
                }
            }
        }

        private void AutpilotAutodockSetBatteriesAndThrusters(bool on)
        {
            foreach (var battery in batteries)
                battery.ChargeMode = on ? ChargeMode.Auto : ChargeMode.Recharge;

            foreach (var dirThrustMap in thrusterDirections)
                foreach (var thruster in dirThrustMap.Value)
                    thruster.Enabled = on;
        }

        private const string enableString = "enable";
        private const string disableString = "disable";

        private void CommandAutopilot(string argument)
        {
            string[] arguments = argument.Split(' ');

            if (arguments.Length == 0)
                return;

            if (arguments[0] == enableString)
            {
                autopilotEnabled = true;
                SetSetting(apEnabledKey, true);
            }
            else if (arguments[0] == disableString)
            {
                autopilotEnabled = false;
                SetSetting(apEnabledKey, false);
            }
            else if (arguments[0] == "auto_off" && arguments.Length == 2)
            {
                if (arguments[1] == enableString)
                {
                    AutoOffEnabled = true;
                    SetSetting(apAutoOffKey, true);
                }
                else if (arguments[1] == disableString)
                {
                    AutoOffEnabled = false;
                    SetSetting(apAutoOffKey, false);
                    ShipIsShutDown = false;
                }
            }
            else
            {
                Echo("Unknown autopilot command");
            }

        }

        private void DisplayAutopilot()
        {
            if (!HasDisplaysForMode(DisplayMode.autopilot)) return;

            var text = GetStringBuilderForScreen();
            CreateHeader(text, "Autopilot");

            if (mainShipController == null)
            {
                text.Append('\n');
                CreateCenteredText(text, requiresMainShipController);
                text.Append('\n');
            }
            else
            {
                CreateChecklistItem(text, "Enabled", autopilotEnabled);

                if (autopilotEnabled) { 
                    CreateChecklistItem(text, "Dampeners", DampenersEnabled);
                    CreateChecklistItem(text, "Piloted", ShipIsControlled);

                    if( SelectedProgramMode != ProgramMode.Main){
                        CreateChecklistItem(text, "Docked", ShipIsDocked);
                    }
                    
                    if (SelectedProgramMode == ProgramMode.Miner)
                    {
                        CreateChecklistItem(text, "Auto off enabled", AutoOffEnabled);

                        if (AutoOffEnabled)
                        {
                            CreateChecklistItem(text, "Is auto off", ShipIsShutDown);
                        }
                    }
                }

            }

            WriteRenderedTextToScreens(DisplayMode.autopilot, text);
        }
        #endregion
        ///////////////////////////////
        #region connectors
        private List<IMyShipConnector> Connectors = new List<IMyShipConnector>();
        private List<IMyLandingGear> LandingGears = new List<IMyLandingGear>();

        private bool ShipIsLanded = false;
        private bool ShipIsDocked = false;

        private void InitConnectors()
        {
            Connectors.Clear();
            LandingGears.Clear();
            GridTerminalSystem.GetBlocksOfType<IMyShipConnector>(Connectors, CurrentGridOnly);
            GridTerminalSystem.GetBlocksOfType<IMyLandingGear>(LandingGears, CurrentGridOnly);
        }

        private void UpdateConnectors()
        {
            ShipIsLanded = false;

            foreach (var landingGear in LandingGears)
            {
                if (landingGear.IsLocked)
                {
                    ShipIsLanded = true;
                    break;
                }
            }

            ShipIsDocked = false;

            if (SelectedProgramMode != ProgramMode.Main)
            {
                foreach (var connector in Connectors)
                {
                    // only use parking enabled connectors as indicator if the ship is docked
                    if (connector.IsParkingEnabled && connector.IsConnected)
                    {
                        ShipIsDocked = true;
                        break;
                    }
                }
            }
        }

        private void DisplayConnectors()
        {
            if (!HasDisplaysForMode(DisplayMode.connectors)) return;

            var text = GetStringBuilderForScreen();

            CreateHeader(text, "Connectors");

            foreach (var connector in Connectors)
            {
                text.Append(connector.CustomName);
                text.Append('\n');

                var status = connector.Status;
                string statusString = status.ToString();

                if (status == MyShipConnectorStatus.Connected)
                {
                    var otherConnector = connector.OtherConnector;
                    statusString = otherConnector.CubeGrid.CustomName;
                }

                CreateTwoColumnText(text, "", statusString);
            }

            WriteRenderedTextToScreens(DisplayMode.connectors, text);
        }

        #endregion
        ///////////////////////////////
        #region jumpdrives
        private List<IMyJumpDrive> JumpDrives = new List<IMyJumpDrive>();

        private float JumpDrivesMaxStoredPower;
        private float JumpDrivesCurrentStoredPower;
        private float JumpDrivesCurrentStoredPercentage;
        private int JumpDrivesNumReady;
        private int JumpDrivesNumJumping;
        private int JumpDrivesNumEnabled;

        private void InitJumpdrives()
        {
            JumpDrives.Clear();

            GridTerminalSystem.GetBlocksOfType<IMyJumpDrive>(JumpDrives, CurrentGridOnly);

            JumpDrivesNumReady = 0;
            JumpDrivesMaxStoredPower = 0;
            foreach (var drive in JumpDrives)
            {
                JumpDrivesMaxStoredPower += drive.MaxStoredPower;
            }
        }

        private void UpdateJumpdrives()
        {
            JumpDrivesCurrentStoredPower = 0;
            JumpDrivesNumReady = 0;
            JumpDrivesNumJumping = 0;
            JumpDrivesNumEnabled = 0;

            foreach (var drive in JumpDrives)
            {
                JumpDrivesCurrentStoredPower += drive.CurrentStoredPower;

                switch (drive.Status)
                {
                    case MyJumpDriveStatus.Ready:
                        JumpDrivesNumReady++;
                        break;
                    case MyJumpDriveStatus.Jumping:
                        JumpDrivesNumJumping++;
                        break;

                }

                if (drive.Enabled)
                {
                    JumpDrivesNumEnabled++;
                }
            }

            JumpDrivesCurrentStoredPercentage = JumpDrivesCurrentStoredPower / JumpDrivesMaxStoredPower;
        }

        private void DisplayJumpdrives()
        {
            if (!HasDisplaysForMode(DisplayMode.jumpdrives)) return;
            var text = GetStringBuilderForScreen();

            CreateHeader(text, "Jump Drives");

            if (JumpDrives.Count == 0)
            {
                CreateCenteredText(text, "No Jump Drives");
            }
            else
            {
                CreateTwoColumnText(text, "Charge:", JumpDrivesCurrentStoredPercentage.ToString(PercentageNumberFormat));
                CreateTwoColumnText(text, "", JumpDrivesCurrentStoredPower.ToString(ThousandSeparatorNumberFormatWithSingleDecimalPlace) + '/' + JumpDrivesMaxStoredPower.ToString(ThousandSeparatorNumberFormatWithSingleDecimalPlace) + " MWh");
                CreateBar(text, JumpDrivesCurrentStoredPercentage, false);
                text.Append('\n');

                CreateTwoColumnText(text, "Ready", JumpDrivesNumReady.ToString() + '/' + JumpDrives.Count.ToString());
                text.Append('\n');

                if (JumpDrivesNumEnabled < JumpDrives.Count)
                {
                    CreateTwoColumnText(text, "Enabled", JumpDrivesNumEnabled.ToString() + '/' + JumpDrives.Count.ToString());
                }

                if (JumpDrivesNumJumping > 0)
                {
                    CreateCenteredText(text, "! Jumping !");
                }
            }

            WriteRenderedTextToScreens(DisplayMode.jumpdrives, text);
        }
        #endregion
        ///////////////////////////////
        #region statusreport
        // Status Report exclusive uses already available data, so no UpdateStatusReport ist necessary
        private void DisplayStatusReport(){
            if (!HasDisplaysForMode(DisplayMode.statusreport)) return;
            var text = GetStringBuilderForScreen();

            CreateHeader(text, "Status Report");

            var hydroStockpileCount = gasStorageNumInStockpileMode[GasType.Hydrogen];
            if( hydroStockpileCount > 0 ){
                var num = hydroStockpileCount < gasStorageTanks[GasType.Hydrogen].Count ? hydroStockpileCount.ToString(ThousandSeparatorNumberFormat) : "all";
                CreateTwoColumnText(text, "Hydrogen Stkpl", num);
            }

            var oxyStockpileCount = gasStorageNumInStockpileMode[GasType.Oxygen];
            if( oxyStockpileCount > 0 ){
                var num = oxyStockpileCount < gasStorageTanks[GasType.Oxygen].Count ? oxyStockpileCount.ToString(ThousandSeparatorNumberFormat) : "all";
                CreateTwoColumnText(text, "Oxygen Stkpl", num);
            }

            if( batteriesNumModeRecharging > 0 ){
                var num = batteriesNumModeRecharging < batteries.Count ? batteriesNumModeRecharging.ToString(ThousandSeparatorNumberFormat) : "All";
                text.Append(num + " Bttrs recharge\n");
            }

            if( batteries.Count > 0 && batteriesChargePercentage < 0.1 ){
                text.Append(batteriesChargePercentage.ToString(PercentageNumberFormat) + " Bttrs low\n");
            }

            // TODO make this a setting
            if( reactors.Count > 0 && GetAmountOfResource(InventoryResourceType.Ingot, InventoryResourceName.Uranium) < 15 ){
                text.Append("Uranium low\n");
            }

            if (SelectedProgramMode != ProgramMode.Fighter && mainShipController != null){
                if( thrustNecessaryDownTotalInN / thrusterInformation[Base6Directions.Direction.Up].maxEffectiveThrust >= 0.9 ){
                    text.Append("Downward thrust limit\n");
                }

                if( thrustLeftNecessaryInN / thrusterInformation[Base6Directions.Direction.Left].maxEffectiveThrust >= 0.9 ){
                    text.Append("Left thrust limit\n");
                }

                if( thrustRightNecessaryInN / thrusterInformation[Base6Directions.Direction.Right].maxEffectiveThrust >= 0.9 ){
                    text.Append("Right thrust limit\n");
                }

                if( thrustFrontNecessaryInN / thrusterInformation[Base6Directions.Direction.Forward].maxEffectiveThrust >= 0.9 ){
                    text.Append("Forward thrust limit\n");
                }

                if( thrustBackNecessaryInN / thrusterInformation[Base6Directions.Direction.Backward].maxEffectiveThrust >= 0.9 ){
                    text.Append("Backward thrust limit\n");
                }
            }

            if( hangarStatus.ContainsValue(DoorStatus.Opening) ){
                text.Append("Hangar(s) opening\n");
            }

            if( hangarStatus.ContainsValue(DoorStatus.Closing) ){
                text.Append("Hangar(s) closing\n");
            }

            if( JumpDrivesNumJumping > 0 ){
                text.Append("Jumping\n");
            }

            WriteRenderedTextToScreens(DisplayMode.statusreport, text);
        }
        #endregion
        ///////////////////////////////
        #region misc
        private void PrefixBlocksWithShortCode(){
            var name = Me.CubeGrid.CustomName;
            
            if( !name.StartsWith("[") || !name.Contains(']') ){
                Echo("No square bracket found");
                return;
            }

            var prefix = name.Substring(0, name.IndexOf(']') + 1);

            var blocks = new List<IMyTerminalBlock>();
            GridTerminalSystem.GetBlocksOfType<IMyTerminalBlock>(blocks, CurrentGridOnly);

            blocks.ForEach((block) => {
                if( !block.CustomName.StartsWith(prefix) ){
                    block.CustomName = prefix + ' ' + block.CustomName;
                }
            });
        }
        #endregion
        ///////////////////////////////
        #region debug
        private void DisplayDebug()
        {
            if (!HasDisplaysForMode(DisplayMode.debug)) return;

            var textSurfaces = GetDisplaysForMode(DisplayMode.debug);

            foreach (var textSurface in textSurfaces)
            {
                var text = GetStringBuilderForScreen();

                CreateHeader(text, "Debug");
                CreateTwoColumnText(text, "Width", DisplayWidth.ToString());
                CreateTwoColumnText(text, "Height", DisplayHeight.ToString());

                CreateTwoColumnText(text, "FontSize", textSurface.FontSize.ToString());
                CreateTwoColumnText(text, "Padding", textSurface.TextPadding.ToString());

                CreateTwoColumnText(text, "Surface size:", textSurface.SurfaceSize.X.ToString() + "/" + textSurface.SurfaceSize.Y.ToString());
                CreateTwoColumnText(text, "Texture size:", textSurface.TextureSize.X.ToString() + "/" + textSurface.TextureSize.Y.ToString());

                textSurface.WriteText(text.ToString());
            }
        }
        #endregion

        // end of script
        #endregion

    }
}
